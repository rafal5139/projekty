﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace RozpoznawanieRas
{
    abstract public class Net
    {
        public Net() { }
        ~Net() { }
        public Data data = new Data();
        public double[][] input;
        abstract public void InitializeData(string dane);
        abstract public void LoadData(string dane);
        abstract public void Train();
        abstract public double TestNet(string test);
        abstract public double Run(Bitmap zdjecie);
        abstract public void SaveNet(string siec);
        abstract public void LoadNet(string siec);

    }
}
