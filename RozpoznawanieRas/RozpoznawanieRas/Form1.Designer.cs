﻿namespace RozpoznawanieRas
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBoxRecognition = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonRecognitionPickPicture = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.textBoxRecognitionResult = new System.Windows.Forms.TextBox();
            this.buttonRecognitionTest = new System.Windows.Forms.Button();
            this.buttonRecognitionClear = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageRecognition = new System.Windows.Forms.TabPage();
            this.buttonRecognitionExit = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.button26 = new System.Windows.Forms.Button();
            this.buttonPageTrain = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.tabPageRBF = new System.Windows.Forms.TabPage();
            this.groupBoxMlpTestImage = new System.Windows.Forms.GroupBox();
            this.buttonRbfTestLoadNet = new System.Windows.Forms.Button();
            this.textBoxRbfTestNetName = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.textBoxRbfTestNumberOfFunction = new System.Windows.Forms.TextBox();
            this.textBoxRbfTestResult = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.buttonRbfTestPickImage = new System.Windows.Forms.Button();
            this.textBoxRbfTestSizeOfInput = new System.Windows.Forms.TextBox();
            this.buttonRbfTestTest = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.pictureBoxRbfImage = new System.Windows.Forms.PictureBox();
            this.textBoxRbfTestSigmum = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.groupBoxRbfTestMany = new System.Windows.Forms.GroupBox();
            this.textBoxRbfTestManyNetName = new System.Windows.Forms.TextBox();
            this.buttonRbfTestManyLoadNet = new System.Windows.Forms.Button();
            this.textBoxRbfTestManyError = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.buttonRbfTestManyTest = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxRbfTestManyData = new System.Windows.Forms.TextBox();
            this.textBoxRbfTestManyNumberOfFuction = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBoxRbfTestManySizeOfInput = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.textBoxRbfTestManySigmum = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.buttonRbfTestManyLoadData = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.textBoxRbfTestManyNumberOfData = new System.Windows.Forms.TextBox();
            this.groupBoxMlpLearn = new System.Windows.Forms.GroupBox();
            this.buttonRbfTrain = new System.Windows.Forms.Button();
            this.textBoxRbfLearnSizeOfInput = new System.Windows.Forms.TextBox();
            this.textBoxRbfLearnNumberOfRadialFunction = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.buttonRbfLearnLoadData = new System.Windows.Forms.Button();
            this.buttonRbfSaveData = new System.Windows.Forms.Button();
            this.textBoxRbfLearnNumberOfIteration = new System.Windows.Forms.TextBox();
            this.textBoxRbfLearnNetName = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxRbfLearnError = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxRbfLearnData = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.textBoxRbfLearnnumberOfData = new System.Windows.Forms.TextBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.button38 = new System.Windows.Forms.Button();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.button36 = new System.Windows.Forms.Button();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.button37 = new System.Windows.Forms.Button();
            this.label35 = new System.Windows.Forms.Label();
            this.pictureBoxHopfield = new System.Windows.Forms.PictureBox();
            this.tabPageMLP = new System.Windows.Forms.TabPage();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.button39 = new System.Windows.Forms.Button();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.button40 = new System.Windows.Forms.Button();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.button41 = new System.Windows.Forms.Button();
            this.tabPageDatabase = new System.Windows.Forms.TabPage();
            this.groupBoxAddBreed = new System.Windows.Forms.GroupBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.label10 = new System.Windows.Forms.Label();
            this.button13 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.button14 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.button15 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.groupBoxAddFace = new System.Windows.Forms.GroupBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.button10 = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.button11 = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button12 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.button17 = new System.Windows.Forms.Button();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.rectangleShape5 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape4 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape3 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.rectangleShape2 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecognition)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPageRecognition.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPageRBF.SuspendLayout();
            this.groupBoxMlpTestImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRbfImage)).BeginInit();
            this.groupBoxRbfTestMany.SuspendLayout();
            this.groupBoxMlpLearn.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHopfield)).BeginInit();
            this.tabPageMLP.SuspendLayout();
            this.tabPageDatabase.SuspendLayout();
            this.groupBoxAddBreed.SuspendLayout();
            this.groupBoxAddFace.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBoxRecognition
            // 
            this.pictureBoxRecognition.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.pictureBoxRecognition.Location = new System.Drawing.Point(6, 64);
            this.pictureBoxRecognition.Name = "pictureBoxRecognition";
            this.pictureBoxRecognition.Size = new System.Drawing.Size(250, 200);
            this.pictureBoxRecognition.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRecognition.TabIndex = 2;
            this.pictureBoxRecognition.TabStop = false;
            this.pictureBoxRecognition.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            this.pictureBoxRecognition.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBoxRecognition.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBoxRecognition.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(652, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // buttonRecognitionPickPicture
            // 
            this.buttonRecognitionPickPicture.Location = new System.Drawing.Point(8, 26);
            this.buttonRecognitionPickPicture.Name = "buttonRecognitionPickPicture";
            this.buttonRecognitionPickPicture.Size = new System.Drawing.Size(108, 24);
            this.buttonRecognitionPickPicture.TabIndex = 4;
            this.buttonRecognitionPickPicture.Text = "Wybierz zdjęcie";
            this.buttonRecognitionPickPicture.UseVisualStyleBackColor = true;
            this.buttonRecognitionPickPicture.Click += new System.EventHandler(this.buttonRecognitionPickPicture_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "JPG (*.jpg)|*.jpg";
            // 
            // textBoxRecognitionResult
            // 
            this.textBoxRecognitionResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRecognitionResult.Location = new System.Drawing.Point(278, 64);
            this.textBoxRecognitionResult.Multiline = true;
            this.textBoxRecognitionResult.Name = "textBoxRecognitionResult";
            this.textBoxRecognitionResult.Size = new System.Drawing.Size(158, 64);
            this.textBoxRecognitionResult.TabIndex = 1;
            // 
            // buttonRecognitionTest
            // 
            this.buttonRecognitionTest.Location = new System.Drawing.Point(308, 157);
            this.buttonRecognitionTest.Name = "buttonRecognitionTest";
            this.buttonRecognitionTest.Size = new System.Drawing.Size(91, 39);
            this.buttonRecognitionTest.TabIndex = 8;
            this.buttonRecognitionTest.Text = "Test";
            this.buttonRecognitionTest.UseVisualStyleBackColor = true;
            this.buttonRecognitionTest.Click += new System.EventHandler(this.buttonRecognitionTest_Click);
            // 
            // buttonRecognitionClear
            // 
            this.buttonRecognitionClear.Location = new System.Drawing.Point(136, 26);
            this.buttonRecognitionClear.Name = "buttonRecognitionClear";
            this.buttonRecognitionClear.Size = new System.Drawing.Size(75, 23);
            this.buttonRecognitionClear.TabIndex = 13;
            this.buttonRecognitionClear.Text = "Clear";
            this.buttonRecognitionClear.UseVisualStyleBackColor = true;
            this.buttonRecognitionClear.Click += new System.EventHandler(this.buttonRecognitionClear_Click);
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox2.Location = new System.Drawing.Point(24, 43);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(176, 20);
            this.textBox2.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageRecognition);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPageRBF);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPageMLP);
            this.tabControl1.Controls.Add(this.tabPageDatabase);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(652, 458);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPageRecognition
            // 
            this.tabPageRecognition.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tabPageRecognition.Controls.Add(this.buttonRecognitionExit);
            this.tabPageRecognition.Controls.Add(this.pictureBoxRecognition);
            this.tabPageRecognition.Controls.Add(this.textBoxRecognitionResult);
            this.tabPageRecognition.Controls.Add(this.buttonRecognitionTest);
            this.tabPageRecognition.Controls.Add(this.buttonRecognitionClear);
            this.tabPageRecognition.Controls.Add(this.buttonRecognitionPickPicture);
            this.tabPageRecognition.Location = new System.Drawing.Point(4, 22);
            this.tabPageRecognition.Name = "tabPageRecognition";
            this.tabPageRecognition.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageRecognition.Size = new System.Drawing.Size(644, 432);
            this.tabPageRecognition.TabIndex = 0;
            this.tabPageRecognition.Text = "Rozpoznanie Rasy";
            // 
            // buttonRecognitionExit
            // 
            this.buttonRecognitionExit.AutoSize = true;
            this.buttonRecognitionExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonRecognitionExit.Location = new System.Drawing.Point(523, 367);
            this.buttonRecognitionExit.Name = "buttonRecognitionExit";
            this.buttonRecognitionExit.Size = new System.Drawing.Size(87, 40);
            this.buttonRecognitionExit.TabIndex = 18;
            this.buttonRecognitionExit.Text = "Zakończ";
            this.buttonRecognitionExit.UseVisualStyleBackColor = true;
            this.buttonRecognitionExit.Click += new System.EventHandler(this.buttonRecognitionExit_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tabPage3.Controls.Add(this.textBox12);
            this.tabPage3.Controls.Add(this.textBox11);
            this.tabPage3.Controls.Add(this.textBox10);
            this.tabPage3.Controls.Add(this.button26);
            this.tabPage3.Controls.Add(this.buttonPageTrain);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.textBox8);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.textBox7);
            this.tabPage3.Controls.Add(this.textBox6);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(644, 432);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Trenuj sieć";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(179, 35);
            this.textBox12.Multiline = true;
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(159, 142);
            this.textBox12.TabIndex = 30;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(33, 175);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(100, 20);
            this.textBox11.TabIndex = 29;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(33, 146);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(100, 20);
            this.textBox10.TabIndex = 28;
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(168, 260);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(75, 23);
            this.button26.TabIndex = 27;
            this.button26.Text = "Run";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.button26_Click);
            // 
            // buttonPageTrain
            // 
            this.buttonPageTrain.Location = new System.Drawing.Point(168, 204);
            this.buttonPageTrain.Name = "buttonPageTrain";
            this.buttonPageTrain.Size = new System.Drawing.Size(75, 23);
            this.buttonPageTrain.TabIndex = 26;
            this.buttonPageTrain.Text = "Train";
            this.buttonPageTrain.UseVisualStyleBackColor = true;
            this.buttonPageTrain.Click += new System.EventHandler(this.button25_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(457, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 25;
            this.label6.Text = "Epoki";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(460, 59);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(47, 20);
            this.textBox8.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(457, 130);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "momentum";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(457, 82);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "eta";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(460, 146);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(47, 20);
            this.textBox7.TabIndex = 20;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(460, 98);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(47, 20);
            this.textBox6.TabIndex = 19;
            // 
            // tabPageRBF
            // 
            this.tabPageRBF.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tabPageRBF.Controls.Add(this.groupBoxMlpTestImage);
            this.tabPageRBF.Controls.Add(this.groupBoxRbfTestMany);
            this.tabPageRBF.Controls.Add(this.groupBoxMlpLearn);
            this.tabPageRBF.Location = new System.Drawing.Point(4, 22);
            this.tabPageRBF.Name = "tabPageRBF";
            this.tabPageRBF.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageRBF.Size = new System.Drawing.Size(644, 432);
            this.tabPageRBF.TabIndex = 3;
            this.tabPageRBF.Text = "Sieć RBF";
            // 
            // groupBoxMlpTestImage
            // 
            this.groupBoxMlpTestImage.Controls.Add(this.buttonRbfTestLoadNet);
            this.groupBoxMlpTestImage.Controls.Add(this.textBoxRbfTestNetName);
            this.groupBoxMlpTestImage.Controls.Add(this.label29);
            this.groupBoxMlpTestImage.Controls.Add(this.label30);
            this.groupBoxMlpTestImage.Controls.Add(this.textBoxRbfTestNumberOfFunction);
            this.groupBoxMlpTestImage.Controls.Add(this.textBoxRbfTestResult);
            this.groupBoxMlpTestImage.Controls.Add(this.label28);
            this.groupBoxMlpTestImage.Controls.Add(this.buttonRbfTestPickImage);
            this.groupBoxMlpTestImage.Controls.Add(this.textBoxRbfTestSizeOfInput);
            this.groupBoxMlpTestImage.Controls.Add(this.buttonRbfTestTest);
            this.groupBoxMlpTestImage.Controls.Add(this.label27);
            this.groupBoxMlpTestImage.Controls.Add(this.pictureBoxRbfImage);
            this.groupBoxMlpTestImage.Controls.Add(this.textBoxRbfTestSigmum);
            this.groupBoxMlpTestImage.Controls.Add(this.label26);
            this.groupBoxMlpTestImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBoxMlpTestImage.Location = new System.Drawing.Point(14, 262);
            this.groupBoxMlpTestImage.Name = "groupBoxMlpTestImage";
            this.groupBoxMlpTestImage.Size = new System.Drawing.Size(616, 164);
            this.groupBoxMlpTestImage.TabIndex = 51;
            this.groupBoxMlpTestImage.TabStop = false;
            this.groupBoxMlpTestImage.Text = "Test dla zdjęcia";
            // 
            // buttonRbfTestLoadNet
            // 
            this.buttonRbfTestLoadNet.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonRbfTestLoadNet.Location = new System.Drawing.Point(125, 39);
            this.buttonRbfTestLoadNet.Name = "buttonRbfTestLoadNet";
            this.buttonRbfTestLoadNet.Size = new System.Drawing.Size(88, 23);
            this.buttonRbfTestLoadNet.TabIndex = 31;
            this.buttonRbfTestLoadNet.Text = "Wczytaj sieć";
            this.buttonRbfTestLoadNet.UseVisualStyleBackColor = true;
            // 
            // textBoxRbfTestNetName
            // 
            this.textBoxRbfTestNetName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRbfTestNetName.Location = new System.Drawing.Point(16, 42);
            this.textBoxRbfTestNetName.Name = "textBoxRbfTestNetName";
            this.textBoxRbfTestNetName.Size = new System.Drawing.Size(100, 20);
            this.textBoxRbfTestNetName.TabIndex = 32;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label29.Location = new System.Drawing.Point(15, 26);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(67, 13);
            this.label29.TabIndex = 33;
            this.label29.Text = "Nazwa sieci:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label30.Location = new System.Drawing.Point(311, 112);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(40, 13);
            this.label30.TabIndex = 44;
            this.label30.Text = "Wynik:";
            // 
            // textBoxRbfTestNumberOfFunction
            // 
            this.textBoxRbfTestNumberOfFunction.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRbfTestNumberOfFunction.Location = new System.Drawing.Point(109, 83);
            this.textBoxRbfTestNumberOfFunction.Name = "textBoxRbfTestNumberOfFunction";
            this.textBoxRbfTestNumberOfFunction.Size = new System.Drawing.Size(73, 20);
            this.textBoxRbfTestNumberOfFunction.TabIndex = 34;
            // 
            // textBoxRbfTestResult
            // 
            this.textBoxRbfTestResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRbfTestResult.Location = new System.Drawing.Point(307, 128);
            this.textBoxRbfTestResult.Name = "textBoxRbfTestResult";
            this.textBoxRbfTestResult.Size = new System.Drawing.Size(101, 20);
            this.textBoxRbfTestResult.TabIndex = 43;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label28.Location = new System.Drawing.Point(106, 67);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(75, 13);
            this.label28.TabIndex = 35;
            this.label28.Text = "Liczba funckji:";
            // 
            // buttonRbfTestPickImage
            // 
            this.buttonRbfTestPickImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonRbfTestPickImage.Location = new System.Drawing.Point(16, 112);
            this.buttonRbfTestPickImage.Name = "buttonRbfTestPickImage";
            this.buttonRbfTestPickImage.Size = new System.Drawing.Size(108, 24);
            this.buttonRbfTestPickImage.TabIndex = 42;
            this.buttonRbfTestPickImage.Text = "Wybierz zdjęcie";
            this.buttonRbfTestPickImage.UseVisualStyleBackColor = true;
            // 
            // textBoxRbfTestSizeOfInput
            // 
            this.textBoxRbfTestSizeOfInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRbfTestSizeOfInput.Location = new System.Drawing.Point(17, 83);
            this.textBoxRbfTestSizeOfInput.Name = "textBoxRbfTestSizeOfInput";
            this.textBoxRbfTestSizeOfInput.Size = new System.Drawing.Size(78, 20);
            this.textBoxRbfTestSizeOfInput.TabIndex = 36;
            // 
            // buttonRbfTestTest
            // 
            this.buttonRbfTestTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonRbfTestTest.Location = new System.Drawing.Point(310, 77);
            this.buttonRbfTestTest.Name = "buttonRbfTestTest";
            this.buttonRbfTestTest.Size = new System.Drawing.Size(75, 30);
            this.buttonRbfTestTest.TabIndex = 41;
            this.buttonRbfTestTest.Text = "Testuj";
            this.buttonRbfTestTest.UseVisualStyleBackColor = true;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label27.Location = new System.Drawing.Point(14, 67);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(86, 13);
            this.label27.TabIndex = 37;
            this.label27.Text = "Rozmiar wejścia:";
            // 
            // pictureBoxRbfImage
            // 
            this.pictureBoxRbfImage.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pictureBoxRbfImage.Location = new System.Drawing.Point(439, 15);
            this.pictureBoxRbfImage.Name = "pictureBoxRbfImage";
            this.pictureBoxRbfImage.Size = new System.Drawing.Size(170, 147);
            this.pictureBoxRbfImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRbfImage.TabIndex = 40;
            this.pictureBoxRbfImage.TabStop = false;
            // 
            // textBoxRbfTestSigmum
            // 
            this.textBoxRbfTestSigmum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRbfTestSigmum.Location = new System.Drawing.Point(195, 83);
            this.textBoxRbfTestSigmum.Name = "textBoxRbfTestSigmum";
            this.textBoxRbfTestSigmum.Size = new System.Drawing.Size(72, 20);
            this.textBoxRbfTestSigmum.TabIndex = 38;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label26.Location = new System.Drawing.Point(195, 67);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(47, 13);
            this.label26.TabIndex = 39;
            this.label26.Text = "Sigmum:";
            // 
            // groupBoxRbfTestMany
            // 
            this.groupBoxRbfTestMany.Controls.Add(this.textBoxRbfTestManyNetName);
            this.groupBoxRbfTestMany.Controls.Add(this.buttonRbfTestManyLoadNet);
            this.groupBoxRbfTestMany.Controls.Add(this.textBoxRbfTestManyError);
            this.groupBoxRbfTestMany.Controls.Add(this.label12);
            this.groupBoxRbfTestMany.Controls.Add(this.buttonRbfTestManyTest);
            this.groupBoxRbfTestMany.Controls.Add(this.label14);
            this.groupBoxRbfTestMany.Controls.Add(this.label15);
            this.groupBoxRbfTestMany.Controls.Add(this.textBoxRbfTestManyData);
            this.groupBoxRbfTestMany.Controls.Add(this.textBoxRbfTestManyNumberOfFuction);
            this.groupBoxRbfTestMany.Controls.Add(this.label21);
            this.groupBoxRbfTestMany.Controls.Add(this.textBoxRbfTestManySizeOfInput);
            this.groupBoxRbfTestMany.Controls.Add(this.label22);
            this.groupBoxRbfTestMany.Controls.Add(this.textBoxRbfTestManySigmum);
            this.groupBoxRbfTestMany.Controls.Add(this.label23);
            this.groupBoxRbfTestMany.Controls.Add(this.buttonRbfTestManyLoadData);
            this.groupBoxRbfTestMany.Controls.Add(this.label24);
            this.groupBoxRbfTestMany.Controls.Add(this.textBoxRbfTestManyNumberOfData);
            this.groupBoxRbfTestMany.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBoxRbfTestMany.Location = new System.Drawing.Point(318, 6);
            this.groupBoxRbfTestMany.Name = "groupBoxRbfTestMany";
            this.groupBoxRbfTestMany.Size = new System.Drawing.Size(314, 250);
            this.groupBoxRbfTestMany.TabIndex = 49;
            this.groupBoxRbfTestMany.TabStop = false;
            this.groupBoxRbfTestMany.Text = "Test dla wielu danych";
            // 
            // textBoxRbfTestManyNetName
            // 
            this.textBoxRbfTestManyNetName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRbfTestManyNetName.Location = new System.Drawing.Point(17, 43);
            this.textBoxRbfTestManyNetName.Name = "textBoxRbfTestManyNetName";
            this.textBoxRbfTestManyNetName.Size = new System.Drawing.Size(100, 20);
            this.textBoxRbfTestManyNetName.TabIndex = 5;
            // 
            // buttonRbfTestManyLoadNet
            // 
            this.buttonRbfTestManyLoadNet.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonRbfTestManyLoadNet.Location = new System.Drawing.Point(153, 40);
            this.buttonRbfTestManyLoadNet.Name = "buttonRbfTestManyLoadNet";
            this.buttonRbfTestManyLoadNet.Size = new System.Drawing.Size(88, 23);
            this.buttonRbfTestManyLoadNet.TabIndex = 4;
            this.buttonRbfTestManyLoadNet.Text = "Wczytaj sieć";
            this.buttonRbfTestManyLoadNet.UseVisualStyleBackColor = true;
            // 
            // textBoxRbfTestManyError
            // 
            this.textBoxRbfTestManyError.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRbfTestManyError.Location = new System.Drawing.Point(115, 223);
            this.textBoxRbfTestManyError.Name = "textBoxRbfTestManyError";
            this.textBoxRbfTestManyError.Size = new System.Drawing.Size(100, 20);
            this.textBoxRbfTestManyError.TabIndex = 6;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.Location = new System.Drawing.Point(112, 207);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 13);
            this.label12.TabIndex = 7;
            this.label12.Text = "Błąd:";
            // 
            // buttonRbfTestManyTest
            // 
            this.buttonRbfTestManyTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonRbfTestManyTest.Location = new System.Drawing.Point(222, 173);
            this.buttonRbfTestManyTest.Name = "buttonRbfTestManyTest";
            this.buttonRbfTestManyTest.Size = new System.Drawing.Size(75, 30);
            this.buttonRbfTestManyTest.TabIndex = 8;
            this.buttonRbfTestManyTest.Text = "Testuj";
            this.buttonRbfTestManyTest.UseVisualStyleBackColor = true;
            this.buttonRbfTestManyTest.Click += new System.EventHandler(this.buttonRbfTestManyTest_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label14.Location = new System.Drawing.Point(14, 28);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "Nazwa sieci:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label15.Location = new System.Drawing.Point(14, 115);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 13);
            this.label15.TabIndex = 13;
            this.label15.Text = "Dane testowe:";
            // 
            // textBoxRbfTestManyData
            // 
            this.textBoxRbfTestManyData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRbfTestManyData.Location = new System.Drawing.Point(17, 132);
            this.textBoxRbfTestManyData.Name = "textBoxRbfTestManyData";
            this.textBoxRbfTestManyData.Size = new System.Drawing.Size(100, 20);
            this.textBoxRbfTestManyData.TabIndex = 14;
            // 
            // textBoxRbfTestManyNumberOfFuction
            // 
            this.textBoxRbfTestManyNumberOfFuction.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRbfTestManyNumberOfFuction.Location = new System.Drawing.Point(127, 87);
            this.textBoxRbfTestManyNumberOfFuction.Name = "textBoxRbfTestManyNumberOfFuction";
            this.textBoxRbfTestManyNumberOfFuction.Size = new System.Drawing.Size(73, 20);
            this.textBoxRbfTestManyNumberOfFuction.TabIndex = 20;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label21.Location = new System.Drawing.Point(124, 71);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(75, 13);
            this.label21.TabIndex = 21;
            this.label21.Text = "Liczba funckji:";
            // 
            // textBoxRbfTestManySizeOfInput
            // 
            this.textBoxRbfTestManySizeOfInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRbfTestManySizeOfInput.Location = new System.Drawing.Point(17, 87);
            this.textBoxRbfTestManySizeOfInput.Name = "textBoxRbfTestManySizeOfInput";
            this.textBoxRbfTestManySizeOfInput.Size = new System.Drawing.Size(73, 20);
            this.textBoxRbfTestManySizeOfInput.TabIndex = 22;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label22.Location = new System.Drawing.Point(18, 71);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(86, 13);
            this.label22.TabIndex = 23;
            this.label22.Text = "Rozmiar wejścia:";
            // 
            // textBoxRbfTestManySigmum
            // 
            this.textBoxRbfTestManySigmum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRbfTestManySigmum.Location = new System.Drawing.Point(232, 87);
            this.textBoxRbfTestManySigmum.Name = "textBoxRbfTestManySigmum";
            this.textBoxRbfTestManySigmum.Size = new System.Drawing.Size(73, 20);
            this.textBoxRbfTestManySigmum.TabIndex = 24;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label23.Location = new System.Drawing.Point(229, 71);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(47, 13);
            this.label23.TabIndex = 25;
            this.label23.Text = "Sigmum:";
            // 
            // buttonRbfTestManyLoadData
            // 
            this.buttonRbfTestManyLoadData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonRbfTestManyLoadData.Location = new System.Drawing.Point(127, 129);
            this.buttonRbfTestManyLoadData.Name = "buttonRbfTestManyLoadData";
            this.buttonRbfTestManyLoadData.Size = new System.Drawing.Size(88, 23);
            this.buttonRbfTestManyLoadData.TabIndex = 26;
            this.buttonRbfTestManyLoadData.Text = "Wczytaj dane";
            this.buttonRbfTestManyLoadData.UseVisualStyleBackColor = true;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label24.Location = new System.Drawing.Point(18, 155);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(79, 13);
            this.label24.TabIndex = 27;
            this.label24.Text = "Liczba danych:";
            // 
            // textBoxRbfTestManyNumberOfData
            // 
            this.textBoxRbfTestManyNumberOfData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRbfTestManyNumberOfData.Location = new System.Drawing.Point(17, 173);
            this.textBoxRbfTestManyNumberOfData.Name = "textBoxRbfTestManyNumberOfData";
            this.textBoxRbfTestManyNumberOfData.Size = new System.Drawing.Size(100, 20);
            this.textBoxRbfTestManyNumberOfData.TabIndex = 28;
            // 
            // groupBoxMlpLearn
            // 
            this.groupBoxMlpLearn.Controls.Add(this.buttonRbfTrain);
            this.groupBoxMlpLearn.Controls.Add(this.textBoxRbfLearnSizeOfInput);
            this.groupBoxMlpLearn.Controls.Add(this.textBoxRbfLearnNumberOfRadialFunction);
            this.groupBoxMlpLearn.Controls.Add(this.label31);
            this.groupBoxMlpLearn.Controls.Add(this.label11);
            this.groupBoxMlpLearn.Controls.Add(this.buttonRbfLearnLoadData);
            this.groupBoxMlpLearn.Controls.Add(this.buttonRbfSaveData);
            this.groupBoxMlpLearn.Controls.Add(this.textBoxRbfLearnNumberOfIteration);
            this.groupBoxMlpLearn.Controls.Add(this.textBoxRbfLearnNetName);
            this.groupBoxMlpLearn.Controls.Add(this.label20);
            this.groupBoxMlpLearn.Controls.Add(this.label13);
            this.groupBoxMlpLearn.Controls.Add(this.textBoxRbfLearnError);
            this.groupBoxMlpLearn.Controls.Add(this.label18);
            this.groupBoxMlpLearn.Controls.Add(this.label19);
            this.groupBoxMlpLearn.Controls.Add(this.textBoxRbfLearnData);
            this.groupBoxMlpLearn.Controls.Add(this.label25);
            this.groupBoxMlpLearn.Controls.Add(this.textBoxRbfLearnnumberOfData);
            this.groupBoxMlpLearn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBoxMlpLearn.Location = new System.Drawing.Point(14, 6);
            this.groupBoxMlpLearn.Name = "groupBoxMlpLearn";
            this.groupBoxMlpLearn.Size = new System.Drawing.Size(298, 250);
            this.groupBoxMlpLearn.TabIndex = 50;
            this.groupBoxMlpLearn.TabStop = false;
            this.groupBoxMlpLearn.Text = "Nauka";
            // 
            // buttonRbfTrain
            // 
            this.buttonRbfTrain.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.buttonRbfTrain.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonRbfTrain.Location = new System.Drawing.Point(202, 28);
            this.buttonRbfTrain.Name = "buttonRbfTrain";
            this.buttonRbfTrain.Size = new System.Drawing.Size(75, 30);
            this.buttonRbfTrain.TabIndex = 0;
            this.buttonRbfTrain.Text = "Trenuj";
            this.buttonRbfTrain.UseVisualStyleBackColor = true;
            this.buttonRbfTrain.Click += new System.EventHandler(this.buttonRbfTrain_Click);
            // 
            // textBoxRbfLearnSizeOfInput
            // 
            this.textBoxRbfLearnSizeOfInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRbfLearnSizeOfInput.Location = new System.Drawing.Point(116, 185);
            this.textBoxRbfLearnSizeOfInput.Name = "textBoxRbfLearnSizeOfInput";
            this.textBoxRbfLearnSizeOfInput.Size = new System.Drawing.Size(95, 20);
            this.textBoxRbfLearnSizeOfInput.TabIndex = 48;
            // 
            // textBoxRbfLearnNumberOfRadialFunction
            // 
            this.textBoxRbfLearnNumberOfRadialFunction.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRbfLearnNumberOfRadialFunction.Location = new System.Drawing.Point(17, 43);
            this.textBoxRbfLearnNumberOfRadialFunction.Name = "textBoxRbfLearnNumberOfRadialFunction";
            this.textBoxRbfLearnNumberOfRadialFunction.Size = new System.Drawing.Size(95, 20);
            this.textBoxRbfLearnNumberOfRadialFunction.TabIndex = 1;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label31.Location = new System.Drawing.Point(114, 169);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(86, 13);
            this.label31.TabIndex = 47;
            this.label31.Text = "Rozmiar wejścia:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label11.Location = new System.Drawing.Point(13, 27);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(126, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Liczba funkcji radialnych:";
            // 
            // buttonRbfLearnLoadData
            // 
            this.buttonRbfLearnLoadData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonRbfLearnLoadData.Location = new System.Drawing.Point(116, 134);
            this.buttonRbfLearnLoadData.Name = "buttonRbfLearnLoadData";
            this.buttonRbfLearnLoadData.Size = new System.Drawing.Size(88, 23);
            this.buttonRbfLearnLoadData.TabIndex = 27;
            this.buttonRbfLearnLoadData.Text = "Wczytaj dane";
            this.buttonRbfLearnLoadData.UseVisualStyleBackColor = true;
            // 
            // buttonRbfSaveData
            // 
            this.buttonRbfSaveData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonRbfSaveData.Location = new System.Drawing.Point(117, 222);
            this.buttonRbfSaveData.Name = "buttonRbfSaveData";
            this.buttonRbfSaveData.Size = new System.Drawing.Size(88, 23);
            this.buttonRbfSaveData.TabIndex = 3;
            this.buttonRbfSaveData.Text = "Zapisz Sieć";
            this.buttonRbfSaveData.UseVisualStyleBackColor = true;
            // 
            // textBoxRbfLearnNumberOfIteration
            // 
            this.textBoxRbfLearnNumberOfIteration.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRbfLearnNumberOfIteration.Location = new System.Drawing.Point(17, 86);
            this.textBoxRbfLearnNumberOfIteration.Name = "textBoxRbfLearnNumberOfIteration";
            this.textBoxRbfLearnNumberOfIteration.Size = new System.Drawing.Size(96, 20);
            this.textBoxRbfLearnNumberOfIteration.TabIndex = 46;
            // 
            // textBoxRbfLearnNetName
            // 
            this.textBoxRbfLearnNetName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRbfLearnNetName.Location = new System.Drawing.Point(17, 224);
            this.textBoxRbfLearnNetName.Name = "textBoxRbfLearnNetName";
            this.textBoxRbfLearnNetName.Size = new System.Drawing.Size(92, 20);
            this.textBoxRbfLearnNetName.TabIndex = 10;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label20.Location = new System.Drawing.Point(14, 70);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(74, 13);
            this.label20.TabIndex = 45;
            this.label20.Text = "Liczba iteracji:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label13.Location = new System.Drawing.Point(14, 208);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "Nazwa sieci:";
            // 
            // textBoxRbfLearnError
            // 
            this.textBoxRbfLearnError.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRbfLearnError.Location = new System.Drawing.Point(196, 87);
            this.textBoxRbfLearnError.Name = "textBoxRbfLearnError";
            this.textBoxRbfLearnError.Size = new System.Drawing.Size(95, 20);
            this.textBoxRbfLearnError.TabIndex = 8;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label18.Location = new System.Drawing.Point(193, 70);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(33, 13);
            this.label18.TabIndex = 9;
            this.label18.Text = "Błąd:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label19.Location = new System.Drawing.Point(14, 114);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(74, 13);
            this.label19.TabIndex = 15;
            this.label19.Text = "Dane uczące:";
            // 
            // textBoxRbfLearnData
            // 
            this.textBoxRbfLearnData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRbfLearnData.Location = new System.Drawing.Point(17, 134);
            this.textBoxRbfLearnData.Name = "textBoxRbfLearnData";
            this.textBoxRbfLearnData.Size = new System.Drawing.Size(96, 20);
            this.textBoxRbfLearnData.TabIndex = 16;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label25.Location = new System.Drawing.Point(14, 169);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(79, 13);
            this.label25.TabIndex = 29;
            this.label25.Text = "Liczba danych:";
            // 
            // textBoxRbfLearnnumberOfData
            // 
            this.textBoxRbfLearnnumberOfData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRbfLearnnumberOfData.Location = new System.Drawing.Point(16, 185);
            this.textBoxRbfLearnnumberOfData.Name = "textBoxRbfLearnnumberOfData";
            this.textBoxRbfLearnnumberOfData.Size = new System.Drawing.Size(95, 20);
            this.textBoxRbfLearnnumberOfData.TabIndex = 30;
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tabPage5.Controls.Add(this.textBox38);
            this.tabPage5.Controls.Add(this.button38);
            this.tabPage5.Controls.Add(this.textBox34);
            this.tabPage5.Controls.Add(this.label36);
            this.tabPage5.Controls.Add(this.button36);
            this.tabPage5.Controls.Add(this.textBox35);
            this.tabPage5.Controls.Add(this.label37);
            this.tabPage5.Controls.Add(this.textBox36);
            this.tabPage5.Controls.Add(this.label38);
            this.tabPage5.Controls.Add(this.label39);
            this.tabPage5.Controls.Add(this.textBox37);
            this.tabPage5.Controls.Add(this.button37);
            this.tabPage5.Controls.Add(this.label35);
            this.tabPage5.Controls.Add(this.pictureBoxHopfield);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(644, 432);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Sieć Hopfielda";
            // 
            // textBox38
            // 
            this.textBox38.Location = new System.Drawing.Point(225, 197);
            this.textBox38.Multiline = true;
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(110, 104);
            this.textBox38.TabIndex = 60;
            // 
            // button38
            // 
            this.button38.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.button38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button38.Location = new System.Drawing.Point(225, 58);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(75, 30);
            this.button38.TabIndex = 59;
            this.button38.Text = "Trenuj";
            this.button38.UseVisualStyleBackColor = true;
            // 
            // textBox34
            // 
            this.textBox34.Location = new System.Drawing.Point(107, 116);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(95, 20);
            this.textBox34.TabIndex = 58;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(105, 100);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(85, 13);
            this.label36.TabIndex = 57;
            this.label36.Text = "Rozmiar wzorca:";
            // 
            // button36
            // 
            this.button36.Location = new System.Drawing.Point(107, 65);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(88, 23);
            this.button36.TabIndex = 54;
            this.button36.Text = "Wczytaj dane";
            this.button36.UseVisualStyleBackColor = true;
            // 
            // textBox35
            // 
            this.textBox35.Location = new System.Drawing.Point(7, 116);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(95, 20);
            this.textBox35.TabIndex = 56;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label37.Location = new System.Drawing.Point(5, 100);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(86, 13);
            this.label37.TabIndex = 55;
            this.label37.Text = "Liczba wzorców:";
            // 
            // textBox36
            // 
            this.textBox36.Location = new System.Drawing.Point(8, 65);
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(96, 20);
            this.textBox36.TabIndex = 53;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(5, 45);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(74, 13);
            this.label38.TabIndex = 52;
            this.label38.Text = "Dane uczące:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(8, 148);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(67, 13);
            this.label39.TabIndex = 51;
            this.label39.Text = "Nazwa sieci:";
            // 
            // textBox37
            // 
            this.textBox37.Location = new System.Drawing.Point(11, 164);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(92, 20);
            this.textBox37.TabIndex = 50;
            // 
            // button37
            // 
            this.button37.Location = new System.Drawing.Point(110, 164);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(88, 23);
            this.button37.TabIndex = 49;
            this.button37.Text = "Zapisz Sieć";
            this.button37.UseVisualStyleBackColor = true;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label35.Location = new System.Drawing.Point(88, 13);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(139, 20);
            this.label35.TabIndex = 42;
            this.label35.Text = "Nauka nowej sieci:";
            // 
            // pictureBoxHopfield
            // 
            this.pictureBoxHopfield.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pictureBoxHopfield.Location = new System.Drawing.Point(374, 65);
            this.pictureBoxHopfield.Name = "pictureBoxHopfield";
            this.pictureBoxHopfield.Size = new System.Drawing.Size(167, 147);
            this.pictureBoxHopfield.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxHopfield.TabIndex = 41;
            this.pictureBoxHopfield.TabStop = false;
            // 
            // tabPageMLP
            // 
            this.tabPageMLP.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tabPageMLP.Controls.Add(this.textBox46);
            this.tabPageMLP.Controls.Add(this.label47);
            this.tabPageMLP.Controls.Add(this.textBox45);
            this.tabPageMLP.Controls.Add(this.label46);
            this.tabPageMLP.Controls.Add(this.textBox43);
            this.tabPageMLP.Controls.Add(this.label44);
            this.tabPageMLP.Controls.Add(this.label45);
            this.tabPageMLP.Controls.Add(this.textBox44);
            this.tabPageMLP.Controls.Add(this.button39);
            this.tabPageMLP.Controls.Add(this.textBox39);
            this.tabPageMLP.Controls.Add(this.label40);
            this.tabPageMLP.Controls.Add(this.button40);
            this.tabPageMLP.Controls.Add(this.textBox40);
            this.tabPageMLP.Controls.Add(this.label41);
            this.tabPageMLP.Controls.Add(this.textBox41);
            this.tabPageMLP.Controls.Add(this.label42);
            this.tabPageMLP.Controls.Add(this.label43);
            this.tabPageMLP.Controls.Add(this.textBox42);
            this.tabPageMLP.Controls.Add(this.button41);
            this.tabPageMLP.Location = new System.Drawing.Point(4, 22);
            this.tabPageMLP.Name = "tabPageMLP";
            this.tabPageMLP.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageMLP.Size = new System.Drawing.Size(644, 432);
            this.tabPageMLP.TabIndex = 5;
            this.tabPageMLP.Text = "Sieć MLP";
            // 
            // textBox46
            // 
            this.textBox46.Location = new System.Drawing.Point(234, 38);
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new System.Drawing.Size(96, 20);
            this.textBox46.TabIndex = 78;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(231, 22);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(62, 13);
            this.label47.TabIndex = 77;
            this.label47.Text = "Momentum:";
            // 
            // textBox45
            // 
            this.textBox45.Location = new System.Drawing.Point(118, 38);
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new System.Drawing.Size(96, 20);
            this.textBox45.TabIndex = 76;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(115, 22);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(26, 13);
            this.label46.TabIndex = 75;
            this.label46.Text = "Eta:";
            // 
            // textBox43
            // 
            this.textBox43.Location = new System.Drawing.Point(15, 38);
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(96, 20);
            this.textBox43.TabIndex = 74;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(12, 22);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(74, 13);
            this.label44.TabIndex = 73;
            this.label44.Text = "Liczba iteracji:";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(294, 145);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(33, 13);
            this.label45.TabIndex = 72;
            this.label45.Text = "Błąd:";
            // 
            // textBox44
            // 
            this.textBox44.Location = new System.Drawing.Point(297, 161);
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new System.Drawing.Size(95, 20);
            this.textBox44.TabIndex = 71;
            // 
            // button39
            // 
            this.button39.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.button39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button39.Location = new System.Drawing.Point(297, 99);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(75, 30);
            this.button39.TabIndex = 70;
            this.button39.Text = "Trenuj";
            this.button39.UseVisualStyleBackColor = true;
            // 
            // textBox39
            // 
            this.textBox39.Location = new System.Drawing.Point(115, 132);
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(95, 20);
            this.textBox39.TabIndex = 69;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(113, 116);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(85, 13);
            this.label40.TabIndex = 68;
            this.label40.Text = "Rozmiar wzorca:";
            // 
            // button40
            // 
            this.button40.Location = new System.Drawing.Point(115, 81);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(88, 23);
            this.button40.TabIndex = 65;
            this.button40.Text = "Wczytaj dane";
            this.button40.UseVisualStyleBackColor = true;
            // 
            // textBox40
            // 
            this.textBox40.Location = new System.Drawing.Point(15, 132);
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new System.Drawing.Size(95, 20);
            this.textBox40.TabIndex = 67;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label41.Location = new System.Drawing.Point(13, 116);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(86, 13);
            this.label41.TabIndex = 66;
            this.label41.Text = "Liczba wzorców:";
            // 
            // textBox41
            // 
            this.textBox41.Location = new System.Drawing.Point(16, 81);
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(96, 20);
            this.textBox41.TabIndex = 64;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(13, 61);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(74, 13);
            this.label42.TabIndex = 63;
            this.label42.Text = "Dane uczące:";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(16, 164);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(67, 13);
            this.label43.TabIndex = 62;
            this.label43.Text = "Nazwa sieci:";
            // 
            // textBox42
            // 
            this.textBox42.Location = new System.Drawing.Point(19, 180);
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(92, 20);
            this.textBox42.TabIndex = 61;
            // 
            // button41
            // 
            this.button41.Location = new System.Drawing.Point(118, 180);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(88, 23);
            this.button41.TabIndex = 60;
            this.button41.Text = "Zapisz Sieć";
            this.button41.UseVisualStyleBackColor = true;
            // 
            // tabPageDatabase
            // 
            this.tabPageDatabase.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tabPageDatabase.Controls.Add(this.groupBoxAddBreed);
            this.tabPageDatabase.Controls.Add(this.groupBoxAddFace);
            this.tabPageDatabase.Location = new System.Drawing.Point(4, 22);
            this.tabPageDatabase.Name = "tabPageDatabase";
            this.tabPageDatabase.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageDatabase.Size = new System.Drawing.Size(644, 432);
            this.tabPageDatabase.TabIndex = 1;
            this.tabPageDatabase.Text = "Baza danych";
            // 
            // groupBoxAddBreed
            // 
            this.groupBoxAddBreed.Controls.Add(this.textBox5);
            this.groupBoxAddBreed.Controls.Add(this.label34);
            this.groupBoxAddBreed.Controls.Add(this.radioButton3);
            this.groupBoxAddBreed.Controls.Add(this.textBox33);
            this.groupBoxAddBreed.Controls.Add(this.textBox4);
            this.groupBoxAddBreed.Controls.Add(this.comboBox6);
            this.groupBoxAddBreed.Controls.Add(this.radioButton4);
            this.groupBoxAddBreed.Controls.Add(this.label10);
            this.groupBoxAddBreed.Controls.Add(this.button13);
            this.groupBoxAddBreed.Controls.Add(this.label9);
            this.groupBoxAddBreed.Controls.Add(this.button14);
            this.groupBoxAddBreed.Controls.Add(this.label5);
            this.groupBoxAddBreed.Controls.Add(this.comboBox2);
            this.groupBoxAddBreed.Controls.Add(this.textBox9);
            this.groupBoxAddBreed.Controls.Add(this.button15);
            this.groupBoxAddBreed.Controls.Add(this.button18);
            this.groupBoxAddBreed.Controls.Add(this.comboBox3);
            this.groupBoxAddBreed.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBoxAddBreed.Location = new System.Drawing.Point(8, 217);
            this.groupBoxAddBreed.Name = "groupBoxAddBreed";
            this.groupBoxAddBreed.Size = new System.Drawing.Size(630, 209);
            this.groupBoxAddBreed.TabIndex = 30;
            this.groupBoxAddBreed.TabStop = false;
            this.groupBoxAddBreed.Text = "Dodaj rasę";
            // 
            // textBox5
            // 
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox5.Location = new System.Drawing.Point(438, 43);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(176, 20);
            this.textBox5.TabIndex = 11;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label34.Location = new System.Drawing.Point(351, 157);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(66, 13);
            this.label34.TabIndex = 28;
            this.label34.Text = "Podaj nową:";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.radioButton3.Location = new System.Drawing.Point(32, 20);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(142, 17);
            this.radioButton3.TabIndex = 4;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Dodaj folder ze zdjęciami";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // textBox33
            // 
            this.textBox33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox33.Location = new System.Drawing.Point(354, 173);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(94, 20);
            this.textBox33.TabIndex = 27;
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox4.Location = new System.Drawing.Point(32, 50);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(176, 20);
            this.textBox4.TabIndex = 10;
            // 
            // comboBox6
            // 
            this.comboBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Items.AddRange(new object[] {
            "BazaTreningowaMLP",
            "BazaTreningowa",
            "BazaTestowa",
            "BazaTestowaMLP"});
            this.comboBox6.Location = new System.Drawing.Point(22, 141);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(121, 21);
            this.comboBox6.TabIndex = 25;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.radioButton4.Location = new System.Drawing.Point(438, 20);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(89, 17);
            this.radioButton4.TabIndex = 4;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Dodaj zdjęcie";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.Location = new System.Drawing.Point(221, 157);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "Wybierz bazę:";
            // 
            // button13
            // 
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button13.Location = new System.Drawing.Point(497, 69);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(102, 24);
            this.button13.TabIndex = 6;
            this.button13.Text = "Wybierz zdjęcie";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(351, 100);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 13);
            this.label9.TabIndex = 23;
            this.label9.Text = "Podaj nową:";
            // 
            // button14
            // 
            this.button14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button14.Location = new System.Drawing.Point(519, 164);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(87, 36);
            this.button14.TabIndex = 10;
            this.button14.Text = "Dodaj";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(218, 100);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Wybierz rasę:";
            // 
            // comboBox2
            // 
            this.comboBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Beagle",
            "Chihuahua",
            "Ibizian Hound",
            "Cairn",
            "Golden Retreiver",
            "Siberian Husky",
            "Basenji",
            "Great Pyrenees",
            "Samoyed ",
            "Pomeranian"});
            this.comboBox2.Location = new System.Drawing.Point(221, 117);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(94, 21);
            this.comboBox2.TabIndex = 9;
            // 
            // textBox9
            // 
            this.textBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox9.Location = new System.Drawing.Point(354, 117);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(94, 20);
            this.textBox9.TabIndex = 19;
            // 
            // button15
            // 
            this.button15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button15.Location = new System.Drawing.Point(51, 76);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(102, 24);
            this.button15.TabIndex = 12;
            this.button15.Text = "Wybierz folder";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button18
            // 
            this.button18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button18.Location = new System.Drawing.Point(22, 177);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(121, 23);
            this.button18.TabIndex = 17;
            this.button18.Text = "Usuń bazę";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // comboBox3
            // 
            this.comboBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "Baza testowa",
            "Baza treningowa"});
            this.comboBox3.Location = new System.Drawing.Point(221, 173);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(94, 21);
            this.comboBox3.TabIndex = 13;
            // 
            // groupBoxAddFace
            // 
            this.groupBoxAddFace.Controls.Add(this.radioButton1);
            this.groupBoxAddFace.Controls.Add(this.textBox2);
            this.groupBoxAddFace.Controls.Add(this.button10);
            this.groupBoxAddFace.Controls.Add(this.label33);
            this.groupBoxAddFace.Controls.Add(this.radioButton2);
            this.groupBoxAddFace.Controls.Add(this.textBox32);
            this.groupBoxAddFace.Controls.Add(this.button11);
            this.groupBoxAddFace.Controls.Add(this.textBox3);
            this.groupBoxAddFace.Controls.Add(this.comboBox5);
            this.groupBoxAddFace.Controls.Add(this.comboBox1);
            this.groupBoxAddFace.Controls.Add(this.label4);
            this.groupBoxAddFace.Controls.Add(this.button12);
            this.groupBoxAddFace.Controls.Add(this.label3);
            this.groupBoxAddFace.Controls.Add(this.comboBox4);
            this.groupBoxAddFace.Controls.Add(this.button17);
            this.groupBoxAddFace.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBoxAddFace.Location = new System.Drawing.Point(8, 6);
            this.groupBoxAddFace.Name = "groupBoxAddFace";
            this.groupBoxAddFace.Size = new System.Drawing.Size(630, 205);
            this.groupBoxAddFace.TabIndex = 29;
            this.groupBoxAddFace.TabStop = false;
            this.groupBoxAddFace.Text = "Dodaj twarz";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.radioButton1.Location = new System.Drawing.Point(430, 20);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(89, 17);
            this.radioButton1.TabIndex = 2;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Dodaj zdjęcie";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button10.Location = new System.Drawing.Point(43, 69);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(102, 24);
            this.button10.TabIndex = 1;
            this.button10.Text = "Wybierz folder";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label33.Location = new System.Drawing.Point(351, 129);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(66, 13);
            this.label33.TabIndex = 24;
            this.label33.Text = "Podaj nową:";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.radioButton2.Location = new System.Drawing.Point(32, 20);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(142, 17);
            this.radioButton2.TabIndex = 3;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Dodaj folder ze zdjęciami";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // textBox32
            // 
            this.textBox32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox32.Location = new System.Drawing.Point(354, 145);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(94, 20);
            this.textBox32.TabIndex = 26;
            // 
            // button11
            // 
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button11.Location = new System.Drawing.Point(489, 69);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(102, 24);
            this.button11.TabIndex = 5;
            this.button11.Text = "Wybierz zdjęcie";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox3.Location = new System.Drawing.Point(430, 43);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(176, 20);
            this.textBox3.TabIndex = 4;
            // 
            // comboBox5
            // 
            this.comboBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Items.AddRange(new object[] {
            "BazaTreningowaMLP",
            "BazaTreningowa",
            "BazaTestowa",
            "BazaTestowaMLP"});
            this.comboBox5.Location = new System.Drawing.Point(22, 130);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(121, 21);
            this.comboBox5.TabIndex = 24;
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Twarz",
            "Brak twarzy"});
            this.comboBox1.Location = new System.Drawing.Point(271, 95);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(94, 21);
            this.comboBox1.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(268, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Rodzaj zdjęcia:";
            // 
            // button12
            // 
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button12.Location = new System.Drawing.Point(519, 154);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(87, 36);
            this.button12.TabIndex = 9;
            this.button12.Text = "Dodaj";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(221, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Wybierz bazę:";
            // 
            // comboBox4
            // 
            this.comboBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            "Baza testowa",
            "Baza treningowa"});
            this.comboBox4.Location = new System.Drawing.Point(221, 145);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(94, 21);
            this.comboBox4.TabIndex = 14;
            // 
            // button17
            // 
            this.button17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button17.Location = new System.Drawing.Point(22, 167);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(121, 23);
            this.button17.TabIndex = 16;
            this.button17.Text = "Usuń bazę";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.TabIndex = 0;
            this.shapeContainer2.TabStop = false;
            // 
            // rectangleShape5
            // 
            this.rectangleShape5.BorderWidth = 2;
            this.rectangleShape5.Location = new System.Drawing.Point(2, 268);
            this.rectangleShape5.Name = "rectangleShape5";
            this.rectangleShape5.Size = new System.Drawing.Size(632, 157);
            // 
            // rectangleShape4
            // 
            this.rectangleShape4.BorderWidth = 2;
            this.rectangleShape4.Location = new System.Drawing.Point(312, 2);
            this.rectangleShape4.Name = "rectangleShape4";
            this.rectangleShape4.Size = new System.Drawing.Size(322, 261);
            // 
            // rectangleShape3
            // 
            this.rectangleShape3.BorderWidth = 2;
            this.rectangleShape3.Location = new System.Drawing.Point(2, 1);
            this.rectangleShape3.Name = "rectangleShape3";
            this.rectangleShape3.Size = new System.Drawing.Size(305, 262);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.TabIndex = 0;
            this.shapeContainer1.TabStop = false;
            // 
            // rectangleShape2
            // 
            this.rectangleShape2.BorderWidth = 2;
            this.rectangleShape2.Location = new System.Drawing.Point(3, 208);
            this.rectangleShape2.Name = "rectangleShape2";
            this.rectangleShape2.Size = new System.Drawing.Size(629, 212);
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.BorderWidth = 2;
            this.rectangleShape1.Location = new System.Drawing.Point(3, 1);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(630, 202);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(652, 482);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.MaximumSize = new System.Drawing.Size(668, 520);
            this.MinimumSize = new System.Drawing.Size(668, 520);
            this.Name = "Form1";
            this.Text = "Aplikacja";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecognition)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPageRecognition.ResumeLayout(false);
            this.tabPageRecognition.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPageRBF.ResumeLayout(false);
            this.groupBoxMlpTestImage.ResumeLayout(false);
            this.groupBoxMlpTestImage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRbfImage)).EndInit();
            this.groupBoxRbfTestMany.ResumeLayout(false);
            this.groupBoxRbfTestMany.PerformLayout();
            this.groupBoxMlpLearn.ResumeLayout(false);
            this.groupBoxMlpLearn.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHopfield)).EndInit();
            this.tabPageMLP.ResumeLayout(false);
            this.tabPageMLP.PerformLayout();
            this.tabPageDatabase.ResumeLayout(false);
            this.groupBoxAddBreed.ResumeLayout(false);
            this.groupBoxAddBreed.PerformLayout();
            this.groupBoxAddFace.ResumeLayout(false);
            this.groupBoxAddFace.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxRecognition;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Button buttonRecognitionPickPicture;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox textBoxRecognitionResult;
        private System.Windows.Forms.Button buttonRecognitionTest;
        private System.Windows.Forms.Button buttonRecognitionClear;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageRecognition;
        private System.Windows.Forms.TabPage tabPageDatabase;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape2;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Button buttonPageTrain;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Button buttonRecognitionExit;
        private System.Windows.Forms.TabPage tabPageRBF;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxRbfLearnNetName;
        private System.Windows.Forms.Button buttonRbfTestManyTest;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxRbfTestManyError;
        private System.Windows.Forms.TextBox textBoxRbfTestManyNetName;
        private System.Windows.Forms.Button buttonRbfTestManyLoadNet;
        private System.Windows.Forms.Button buttonRbfSaveData;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxRbfLearnNumberOfRadialFunction;
        private System.Windows.Forms.Button buttonRbfTrain;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPageMLP;
        private System.Windows.Forms.TextBox textBoxRbfLearnData;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBoxRbfLearnError;
        private System.Windows.Forms.TextBox textBoxRbfTestManyData;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBoxRbfTestManyNumberOfFuction;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBoxRbfTestManySizeOfInput;
        private System.Windows.Forms.TextBox textBoxRbfLearnnumberOfData;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBoxRbfTestManyNumberOfData;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button buttonRbfTestManyLoadData;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBoxRbfTestManySigmum;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBoxRbfTestSigmum;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBoxRbfTestSizeOfInput;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox textBoxRbfTestNumberOfFunction;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBoxRbfTestNetName;
        private System.Windows.Forms.Button buttonRbfTestLoadNet;
        private System.Windows.Forms.Button buttonRbfTestTest;
        private System.Windows.Forms.PictureBox pictureBoxRbfImage;
        private System.Windows.Forms.TextBox textBoxRbfLearnNumberOfIteration;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBoxRbfTestResult;
        private System.Windows.Forms.Button buttonRbfTestPickImage;
        private System.Windows.Forms.TextBox textBoxRbfLearnSizeOfInput;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button buttonRbfLearnLoadData;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape5;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape4;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape3;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.PictureBox pictureBoxHopfield;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.GroupBox groupBoxMlpLearn;
        private System.Windows.Forms.GroupBox groupBoxAddBreed;
        private System.Windows.Forms.GroupBox groupBoxAddFace;
        private System.Windows.Forms.GroupBox groupBoxRbfTestMany;
        private System.Windows.Forms.GroupBox groupBoxMlpTestImage;
    }
}

