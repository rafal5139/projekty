﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;

namespace RozpoznawanieRas
{
    public class Hopfield:Net
    {
        public int Bitplane {get; set;}
        public int NumbersOfneurons { get; set; }
        public int NumberOfPatterns {get; set; }
        public static string file = @"TestRas.txt";
        public double[][] Weigths;
        public int[][] pattern;
        public int[][] image;
        public int[,] output;
        int[] dataFromFile;
        public List<int> randNeuron= new List<int>();
        public List<string> breeds=new List<string>();
        public Hopfield(int neurons=400,int layers=8,int patterns=0) {
            NumbersOfneurons = neurons;
            Bitplane = layers;
            NumberOfPatterns = patterns;
        }
        ~Hopfield() { }
        public void CreateTableOfPattrns()
        {
            pattern = new int[Bitplane][];
            for (int i = 0; i < Bitplane; i++)
                pattern[i] = new int[NumbersOfneurons*NumberOfPatterns];
        }
        public void CreateWeightsTable()
        {
            Weigths = new double[Bitplane][];
            for (int i = 0; i < Bitplane; i++)
            {
                Weigths[i] = new double[NumbersOfneurons*NumbersOfneurons];
            }
        }
        public void CreateImageTable()
        {
            image = new int[Bitplane][];
            for (int i = 0; i < Bitplane; i++)
            {
                image[i] = new int[NumbersOfneurons*NumberOfPatterns];
            }
        }
        public void CreateDataTable()
        {
             dataFromFile = new int[NumbersOfneurons*NumberOfPatterns]; 
        }
        public void CreateOutputTable()
        {
            output = new int[Bitplane, NumbersOfneurons];
        }
        public void InicjujTablice()
        {
            CreateDataTable();
            CreateWeightsTable();
            CreateImageTable();
            CreateTableOfPattrns();
            CreateOutputTable();
        }
        override public void InitializeData(string data)
        {
            NumberOfPatterns = base.data.GetNumberOfLines(data) / 2;
            NumberOfData(data);
            InicjujTablice();
            breeds.Clear();
            LoadPatterns(data);
            CreateBitplane(dataFromFile, "pattern");
        }
        override public void LoadData(string data)
        {
            int p = 0;
            using (StreamReader sr = File.OpenText(data))
            {
                string[] tab = new string[5];
                string s = "";
                int i = 0;
                while ((s = sr.ReadLine()) != null && i != NumberOfPatterns)
                {
                    tab = s.Split(' ');
                    for (int j = 0; j < NumbersOfneurons; j++)
                        dataFromFile[j + NumbersOfneurons * p] = int.Parse(tab[j]);
                    p++;

                    i++;
                }
                CreateBitplane(dataFromFile, "zdjecie");
                sr.Close();
            }
        }
        public void LoadPatterns(string data)
        {
            int p = 0;
            using (StreamReader sr = File.OpenText(data))
            {
                string[] tab = new string[5];
                string s = "";
                int i = 0;
                while ((s = sr.ReadLine()) != null && i != NumberOfPatterns)
                {
                    tab = s.Split(' ');
                    for (int j = 0; j < NumbersOfneurons; j++)
                        dataFromFile[j + NumbersOfneurons * p] = int.Parse(tab[j]);
                    if ((s = sr.ReadLine()) != null)
                    {                      
                        tab = s.Split(' ');
                        breeds.Add(tab[0]);
                    }
                    p++;
                    i++;
                }
                sr.Close();
            }
        }
        public void MixListOfNeurons(int numberOfNeurons)
        {
            Random rand=new Random();
            int count=0,value;
            for (int i = 0; i < numberOfNeurons; i++)
                randNeuron.Add(i);
            for (int i = 0; i < numberOfNeurons; i++)
            {
                count= rand.Next(0,numberOfNeurons-1);
                value=randNeuron[count];
                randNeuron.RemoveAt(count);
                count= rand.Next(0,numberOfNeurons-1);
                randNeuron.Insert(count,value);
            }
        }
        public void NumberOfBitplane(string[] tab)
        {
            int max = int.Parse(tab[0]);
            int bit=1,j=0;
            for (int i = 1; i < NumbersOfneurons; i++)
                if (int.Parse(tab[i]) > max)
                    max = int.Parse(tab[i]);
            do
            {
                bit *= 2;
                j++;
            }
            while (bit-1 < max);
            Bitplane = j;
        }
        public void NumberOfData(string data)
        {
            int sum = 0;
            using (StreamReader sr = File.OpenText(data))
            {
                string[] tab = new string[5];
                string s = "";
                if ((s = sr.ReadLine()) != null)
                {
                    if ((s = sr.ReadLine()) != null)
                    {
                        tab = s.Split(' ');
                        if(tab[tab.Length-1]=="")
                        sum = tab.Length-1;
                        else
                        sum = tab.Length;
                        NumbersOfneurons = sum;
                        NumberOfBitplane(tab);
                    }
                }
                sr.Close();
            }
        }
        public void CreateBitplane(int[] data, string table)
        {
            string s;
            int[] bits;
            for (int j = 0; j < NumbersOfneurons * NumberOfPatterns; j++)
            {
                s = Convert.ToString(data[j], 2);
                bits = s.PadLeft(8, '0').Select(c => int.Parse(c.ToString())).ToArray();
                for (int i = 0; i < Bitplane; i++)
                {
                    if (table == "pattern")
                        if (bits[7-i] == 1)
                            pattern[i][j] = 1;
                        else
                            pattern[i][j] = -1;
                    else
                        if (bits[7-i] == 1)
                            image[i][j] = 1;
                        else
                            image[i][j] =-1;
                }
            }           
        }
        public void CalculateWeights()
        {
            double sum=0;
            for(int b=0;b < Bitplane;b++)
                for(int j=0;j<NumbersOfneurons;j++)
                    for(int i=j;i<NumbersOfneurons;i++)
                        if(i==j)
                            Weigths[b][j*NumbersOfneurons+i]=0;
                        else
                        {
                            sum=0;
                            for(int k=0;k<NumberOfPatterns;k++)
                                sum += pattern[b][k*NumbersOfneurons+i]*pattern[b][k*NumbersOfneurons+j];
                            Weigths[b][j*NumbersOfneurons+i]=sum/NumbersOfneurons;
                            Weigths[b][i*NumbersOfneurons+j]=sum/NumbersOfneurons;
                        }
        }
        override public void Train()
        {
            InitializeData(file);
            CalculateWeights();
        }
        override public double TestNet(string test)
        {
            return 0;
        }
        override public double Run(Bitmap image)
        {
            int result,j=0,k=0;
            int noChanges = 0,loop = 0;
            double sum = 0;
            data.SaveImage(image, @"ZdjecieRasy.txt");
            LoadData(@"ZdjecieRasy.txt");
           // WymieszajListeNeuronow(LiczbaNeuronow);

            int[,] neuron_prev = new int[Bitplane,NumbersOfneurons];
            //int poprawnie = 0;
           // for (int z = 0; z < 10000; z++)
            //{
                //WymieszajListeNeuronow(LiczbaNeuronow);
                for (int b = 0; b < Bitplane; b++)
                    for (int i = 0; i < NumbersOfneurons; i++)
                    {
                    output[b, i] = this.image[b][i];
                        neuron_prev[b, i] = output[b, i];
                    }

                Random rand = new Random();
                for (int b = 0; b < Bitplane; b++)
                {
                    noChanges = 0;
                    loop = 0;
                    while (noChanges < 25 && loop < 100)
                    {
                        for (k = 0; k < NumbersOfneurons; k++)
                        {
                            sum = 0;
                            //j = RandNeuron[k];
                            j = rand.Next(0, NumbersOfneurons-1);
                            for (int i = 0; i < NumbersOfneurons; i++)
                                if (i != j)
                                    sum += output[b, i] * Weigths[b][j * NumbersOfneurons + i];
                            if (sum > 0)
                                output[b, j] = 1;
                            else
                                output[b, j] = -1;
                        }
                        bool zmiana = false;
                        for (k = 0; k < NumbersOfneurons; k++)
                            if (output[b, k] != neuron_prev[b, k])
                                zmiana = true;
                        if (zmiana == false)
                            noChanges++;
                        else
                            noChanges = 0;

                        for (k = 0; k < NumbersOfneurons; k++)
                            neuron_prev[b, k] = output[b, k];

                        loop++;
                    }
                //}
                //if (OdlegloscHamminga(0) == 0)
                 //   poprawnie++;
            }

            result = FindPattern();
            return result;
        }
        public int FindPattern()
        {
            int min = (Bitplane*NumbersOfneurons)/2, najlepszy = -1, roznica = 0;
            for (int i = 0; i < NumberOfPatterns; i++)
            {
                roznica = HammingDistance(i);
                if (roznica < min)
                {
                    najlepszy = i;
                    min = roznica;
                }
            }
            return najlepszy;
        }
        public int HammingDistance(int wzorzec)
        {
            int roznica = 0;
                for (int b = 0; b < Bitplane; b++)
                    for (int j = 0; j < NumbersOfneurons; j++)
                    {
                        if (pattern[b][wzorzec * NumbersOfneurons + j] != output[b,j])
                            roznica++;
                    }
            return roznica;
        }
        override public void SaveNet(string siec)
        { 
        }
        override public void LoadNet(string siec)
        {
        }
    }
}
