﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AForge;
using AForge.Imaging;
using AForge.Imaging.Formats;
using AForge.Imaging.Filters;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
namespace RozpoznawanieRas
{
    class Image
    {
        public int width;
        public int height;
        Bitmap newImage;
        public Rectangle section;
        public Image()
        {
            width = 20;
            height = 20;
        }
        ~Image() { }
        public void CreateSection(int width, int height,int x0,int y0)
        {
            section = new Rectangle(x0,y0,width,height);
        }
        public void CreateBitmap(int width, int height)
        {
            newImage = new Bitmap(width, height);
        }
        public Bitmap ResizeImage( Bitmap imgToResize, int width, int height)
        {
            // create filter
            ResizeBilinear filter = new ResizeBilinear(width, height);
            // apply the filter
            Bitmap newImage = filter.Apply(imgToResize);
            return newImage;
        }
        public Bitmap CutImage(Bitmap imgToCut, int width, int height, int x0,int y0)
        {
            CreateBitmap(width,height);
            CreateSection(width, height,x0,y0);
            Graphics g = Graphics.FromImage(newImage);

            // Draw the given area (section) of the source image
            // at location 0,0 on the empty bitmap (bmp)
            g.DrawImage(imgToCut, 0, 0, section, GraphicsUnit.Pixel);

            return newImage;
        }
        public Bitmap DrawFrame(Bitmap imgToDraw, int width, int height, int x0,int y0)
        {
             Graphics g = Graphics.FromImage(imgToDraw);
             g.DrawRectangle(Pens.White, x0, y0, width, height);
           // g.DrawLine(new Pen(Brushes.White, 4), new System.Drawing.Point(x0, y0), new System.Drawing.Point(x0, y0-height));
            //g.DrawLine(new Pen(Brushes.White, 4), new System.Drawing.Point(x0, y0), new System.Drawing.Point(width+x0, y0));
           // g.DrawLine(new Pen(Brushes.White, 4), new System.Drawing.Point(x0, y0-height), new System.Drawing.Point(width+x0, y0-height));
           // g.DrawLine(new Pen(Brushes.White, 4), new System.Drawing.Point(width+x0, y0), new System.Drawing.Point(width+x0, y0-height));
            return imgToDraw;
        }
        public Bitmap HistEqual(Bitmap imgToEqual)
        {
            // create filter
            HistogramEqualization filter = new HistogramEqualization();
            // process image
            filter.ApplyInPlace(imgToEqual);
            return imgToEqual;
        }
        public Bitmap RotateImg(Bitmap imgToRotate, int angle)
        {
            // create filter - rotate for 30 degrees keeping original image size
            RotateBicubic filter = new RotateBicubic(angle, true);
            // apply the filter
            Bitmap newImage = filter.Apply(imgToRotate);
            return newImage;
        }
        public Bitmap Greyscale(Bitmap imgtoGrey)
        {
            // create grayscale filter (BT709)
            Grayscale filter = new Grayscale(0.2125, 0.7154, 0.0721);
            // apply the filter
            Bitmap grayImage = filter.Apply(imgtoGrey);
            return grayImage;
        }
        public Bitmap Normalization(Bitmap imgtoNorm)
        {
            // create filter
            ContrastStretch filter = new ContrastStretch();
            // process image
            filter.ApplyInPlace(imgtoNorm);
            return imgtoNorm;
        }
        public Bitmap Sharpen(Bitmap imgToSharp)
        {
            // create filter
            Sharpen filter = new Sharpen();
            // apply the filter
            filter.ApplyInPlace(imgToSharp);
            return imgToSharp;
        }
        public Bitmap BrightnessCorrection(Bitmap imgToCorrect, int value)
        {           
            // create filter
            BrightnessCorrection filter = new BrightnessCorrection(value);
            // apply the filter
            filter.ApplyInPlace(imgToCorrect);
            return imgToCorrect;
        }
        public Bitmap BlackAndWhite(Bitmap imgToBinary)
        {
            // create filter
            OtsuThreshold filter = new OtsuThreshold();
            // apply the filter
            filter.ApplyInPlace(imgToBinary);
            return imgToBinary;
        }
        public Bitmap Dithering(Bitmap image)
        {
            // create filter
            FloydSteinbergDithering filter = new FloydSteinbergDithering();
            // apply the filter
            filter.ApplyInPlace(image);
            return image;
        }
        public Bitmap Modify(Bitmap imgToModify)
        {
            Bitmap pom = imgToModify;

            pom = Greyscale(pom);
            pom = BrightnessCorrection(pom, HowBright(pom));
            pom = Normalization(pom);
            pom = HistEqual(pom);
            //pom = Dithering(pom);
            pom = ResizeImage(pom, 20, 20);
            return pom;
        }
        public Bitmap ModifyRasa(Bitmap imgToModify)
        {
            Bitmap pom = imgToModify;

            pom = Greyscale(pom);
            pom = BrightnessCorrection(pom, HowBright(pom));
            pom = Normalization(pom);
           // pom = HistEqual(pom);
            //pom = Dithering(pom);
            pom = ResizeImage(pom, 20, 20);
            return pom;
        }
        public int HowBright(Bitmap image)
        {
            int ratio = 0, sum = 0;
            byte[] wczyt;
            wczyt=BitmapToByteArray(image);
            for(int i=0;i<wczyt.Length;i++)
                sum += wczyt[i];
            sum = sum / wczyt.Length;
            ratio=((255-sum) * 100) / 255;
            return ratio;
        }
        public byte[] BitmapToByteArray(Bitmap bitmap)
        {
            BitmapData bmpdata = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, bitmap.PixelFormat);
            int numbytes = bmpdata.Stride * bitmap.Height;
            byte[] bytedata = new byte[numbytes];
            IntPtr ptr = bmpdata.Scan0;

            Marshal.Copy(ptr, bytedata, 0, numbytes);

            bitmap.UnlockBits(bmpdata);

            return bytedata;
        }
    }
    
}
