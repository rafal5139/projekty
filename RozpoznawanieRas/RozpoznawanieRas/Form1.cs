﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace RozpoznawanieRas
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Image image = new Image();
        Data data = new Data();
        MLP net = new MLP();
        Hopfield hopfield = new Hopfield();
        RBF radial = new RBF();
        Bitmap img, oryginal;
        ConvolutionNet conv = new ConvolutionNet();
        Double error = 0,delta=0,result=0;
        private Rectangle Rect = new Rectangle();
        private System.Drawing.Point latestPoint;
        Graphics g;
        private string file;
        private void LoadImage(string file)
        {
            oryginal = (Bitmap)System.Drawing.Image.FromFile(file);
            pictureBoxRecognition.Image = oryginal;
            img = oryginal;
        }

        private void buttonRecognitionPickPicture_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    file = openFileDialog1.FileName;
                    LoadImage(file);
                    textBox2.Text = openFileDialog1.FileName;
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show("Invalid image: " + ex.Message, "Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch
                {
                    MessageBox.Show("Failed loading the image", "Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                double wynik = -1;
                radial.Train();
                wynik = radial.Run();
                if (wynik < 0.0001)
                    wynik = 0;
                textBoxRecognitionResult.Text = wynik.ToString();
                //pictureBox1.Image = obrazek.DrawFrame(img, 80, 80, 200, 200);
                //img = (Bitmap)pictureBox1.Image;
            }
            catch
            {
                MessageBox.Show("Failed loading the image", "Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                double result = -1;
                result = radial.Run();
                if (result < 0.0001)
                    result = 0;
                textBoxRecognitionResult.Text = result.ToString();

                //textBox1.Text = siec.Trenuj(@"DaneDoTwarzy.txt").ToString();
                //textBox1.Text += "\r\n";
                //Net nowa = siec;
                //textBox1.Text +=nowa.TestujSiec(@"Test.txt").ToString()+"%";
                //textBox1.Text += "\r\n";
                //siec.ZapiszSiec(@"Siec.txt");
                
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("Invalid image: " + ex.Message, "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch
            {
                MessageBox.Show("Failed loading the image", "Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonRecognitionTest_Click(object sender, EventArgs e)
        {
            try
            {
                //pictureBox1.Image = obrazek.CutImage(img, 50, 50, 0, 0);  // 260, 200, 120,80
                //img = (Bitmap)pictureBox1.Image;
                net.LoadNet(@"Siec.txt");
                result = net.Run(oryginal);

                textBoxRecognitionResult.Text = result.ToString();
                textBoxRecognitionResult.Text += "\r\n";
                //if (wynik < 0)
                    //textBox1.Text = "To nie twarz";
               // else
                  //  textBox1.Text = "To twarz !!!!!";
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("Invalid image: " + ex.Message, "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch
            {
                MessageBox.Show("Failed loading the image", "Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                data.SaveImage(img, @"Zdjecie213123123.txt");
                //pictureBox1.Image.Save(@"C:\Users\Rafal\Desktop\test.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            catch
            {
                MessageBox.Show("Failed loading the image", "Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                net.Train(@"DaneDoTwarzy.txt");
                error = net.TestNet(@"Test.txt");
                textBoxRecognitionResult.Text = error.ToString();

                //for (int i = 0; i < 2; i++)
               // {
                //    siec.Trenuj(@"DaneDoTwarzy.txt");
                //    delta= siec.TestujSiec(@"Test.txt") - blad;
                //    blad = delta + blad;
                 //   textBox1.Text += blad.ToString();
                 //   textBox1.Text += "\r\n";
                    //if (delta >= 0 && delta <=0.001)
                       // siec.ZmienNastawy(1);
                    //if (delta < 0)
                        //siec.ZmienNastawy(-1);
               // }
                    
                    net.SaveNet(@"Siec.txt");
              
                //data.StworzBaze();
                //data.WymieszajBaze();
                //data.StworzDane();
            }
            catch
            {
                MessageBox.Show("Failed loading the image", "Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                net.LoadNet(@"Siec.txt");
                textBoxRecognitionResult.Text += net.TestNet(@"BazaTestowa.txt").ToString()+"%";
                textBoxRecognitionResult.Text += "\r\n";
                
                //siec.ZapiszSiec(@"Siec.txt");
                //pictureBox1.Image = obrazek.Greyscale(img);
                //img = (Bitmap)pictureBox1.Image;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("Invalid image: " + ex.Message, "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch
            {
                MessageBox.Show("Failed loading the image", "Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                double zmienna=0;
                net.LoadNet(@"Siec.txt");
                //siec.ZmienNastawy(10);
                //do
                //{
                    textBoxRecognitionResult.Text += net.Train(@"DaneDoTwarzy.txt").ToString();
                    textBoxRecognitionResult.Text += "\r\n";
                    zmienna = net.TestNet(@"Test.txt");
                    textBoxRecognitionResult.Text += zmienna.ToString() + "%";
                    textBoxRecognitionResult.Text += "\r\n";
                    delta = zmienna - result;
                    //if(delta <= 0)
                       // siec.eta = siec.eta * 0.8;
                    result = zmienna;
                   
               // } while (delta > 0.0001);

                //siec.ZmienNastawy(8);
                //textBox1.Text += "\r\n";
                //siec.Trenuj(@"DaneDoTwarzy.txt");
                //textBox1.Text += siec.TestujSiec(@"Test.txt").ToString();

                net.SaveNet(@"Siec.txt");
                //siec.Trenuj();
                //siec.ZapiszSiec(@"Siec.txt");
                //pictureBox1.Image = obrazek.HistEqual(img);
                //img = (Bitmap)pictureBox1.Image;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("Invalid image: " + ex.Message, "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch
            {
                MessageBox.Show("Failed loading the image", "Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                // Remember the location where the button was pressed
                latestPoint = e.Location;
                //pictureBox1.Invalidate();
                Invalidate();
            }
        }
        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                using (g = pictureBoxRecognition.CreateGraphics())
                {
                    Rect.Location = new System.Drawing.Point(
                       Math.Min(latestPoint.X, e.Location.X),
                       Math.Min(latestPoint.Y, e.Location.Y));
                    Rect.Size = new Size(
                        Math.Abs(latestPoint.X - e.Location.X),
                        Math.Abs(latestPoint.Y - e.Location.Y));
                    g.DrawRectangle(Pens.White, Rect.Location.X, Rect.Location.Y, Rect.Size.Width, Rect.Size.Height);

                }
            }
            pictureBoxRecognition.Invalidate();
        }
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {

            using (Pen pen = new Pen(Color.White, 1))
            {
                e.Graphics.DrawRectangle(pen, Rect);
            }
        }
        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                using (g = pictureBoxRecognition.CreateGraphics())
                {
                    g.DrawLine(Pens.White, latestPoint.X, latestPoint.Y, latestPoint.X, e.Location.Y);
                    g.DrawLine(Pens.White, latestPoint.X, latestPoint.Y, e.Location.X, latestPoint.Y);
                    g.DrawLine(Pens.White, latestPoint.X, e.Location.Y, e.Location.X, e.Location.Y);
                    g.DrawLine(Pens.White, e.Location.X, latestPoint.Y, e.Location.X, e.Location.Y);
                }
            }
        }

        private void buttonRecognitionClear_Click(object sender, EventArgs e)
        {
            pictureBoxRecognition.Image = null;
            img = null;
            //pictureBox1.Invalidate();
            
            textBoxRecognitionResult.Text = "";
            pictureBoxRecognition.Image = oryginal;
            img = oryginal;

        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox2.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox4.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                file = openFileDialog1.FileName;
                textBox3.Text = openFileDialog1.FileName;
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {

                file = openFileDialog1.FileName;
                textBox5.Text = openFileDialog1.FileName;
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            int value = 0;
            string file = " ";
            if (!radioButton1.Checked && !radioButton2.Checked)
            {
                MessageBox.Show("Nie wybrano opcji", "Error",
              MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (comboBox1.Text == "Twarz")
                    value = 1;
                if (comboBox1.Text == "Brak twarzy")
                    value = -1;
                if (value == 0)
                {
                    MessageBox.Show("Nie wybrano wartości", "Error",
                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    if (comboBox4.Text == "Baza testowa")
                        file = @"Test.txt";
                    if (comboBox4.Text == "Baza treningowa")
                        file = @"DaneDoTwarzy.txt";
                    if (file == " ")
                    {
                        MessageBox.Show("Nie wybrano pliku", "Error",
                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                        try
                        {
                            if (radioButton1.Checked)
                                data.AddImageToMLPDatabase((Bitmap)System.Drawing.Image.FromFile(textBox3.Text), value, file);
                            if (radioButton2.Checked)
                            {
                                data.AddImageFolder(textBox2.Text + "\\", value, file);
                            }

                        }
                        catch
                        {
                            MessageBox.Show("Błąd przy wczytaniu zdjęcia", "Error",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                }
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            string value = "0";
            string file = " ";
            if (!radioButton3.Checked && !radioButton4.Checked)
            {
                MessageBox.Show("Nie wybrano opcji", "Error",
              MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (textBox9.Text != "")
                    value = textBox9.Text;
                else
                   value = comboBox2.Text;
                if (value == "")
                {
                    MessageBox.Show("Nie wybrano rasy", "Error",
                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    if (comboBox3.Text == "Baza testowa")
                        file = @"TestRas.txt";
                    if (comboBox3.Text == "Baza treningowa")
                        file = @"DaneDoRas.txt";
                    if (file == " ")
                    {
                        MessageBox.Show("Nie wybrano pliku", "Error",
                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                        try
                        {
                            if (radioButton4.Checked)
                                data.AddImageToDatabase((Bitmap)System.Drawing.Image.FromFile(textBox5.Text), value, file);
                            if (radioButton3.Checked)
                            {
                                data.AddImageFolder(textBox4.Text + "\\", value, file);
                            }

                        }
                        catch
                        {
                            MessageBox.Show("Błąd przy wczytaniu zdjęcia", "Error",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                }
            } 
        }

        private void button20_Click(object sender, EventArgs e)
        {
            if(textBox6.Text != "")
               net.eta = double.Parse(textBox6.Text);
            if (textBox7.Text != "")
            net.momentum = double.Parse(textBox7.Text);
            if (textBox8.Text != "")
                net.iteration = int.Parse(textBox8.Text);
        }

        private void button17_Click(object sender, EventArgs e)
        {
            File.Delete(@"BazaDoTwarzy.txt");
        }
        private void button21_Click(object sender, EventArgs e)
        {
            Bitmap pom = img;
            pom = image.HistEqual(pom);

            pictureBoxRecognition.Image = pom;
            img = (Bitmap)pictureBoxRecognition.Image;
        }

        private void button22_Click(object sender, EventArgs e)
        {   
 

        }

        private void button23_Click(object sender, EventArgs e)
        {
            pictureBoxRecognition.Image = image.ModifyRasa(img);
            img = (Bitmap)pictureBoxRecognition.Image;
        }

        private void button24_Click(object sender, EventArgs e)
        {
            Bitmap pom = img;
            pom = image.Greyscale(pom);
            pom = image.BrightnessCorrection(pom, 50);
            pom = image.Normalization(pom);
           // pom = obrazek.HistEqual(pom);

            pictureBoxRecognition.Image = pom;
            img = (Bitmap)pictureBoxRecognition.Image;
        }

        private void button18_Click(object sender, EventArgs e)
        {
            File.Delete(@"DaneDoRas.txt");
        }

        private void buttonRbfTrain_Click(object sender, EventArgs e)
        {
            radial.Train();
        }

        private void buttonRbfTestManyTest_Click(object sender, EventArgs e)
        {
            textBoxRbfTestManyData.Text = radial.Run().ToString();
        }

        private void button25_Click(object sender, EventArgs e)
        {
            hopfield.Train();
        }

        private void button26_Click(object sender, EventArgs e)
        {
            int wynik;
            string rasa;
            wynik = Convert.ToInt32(hopfield.Run(img));
            if (wynik >= 0)
            {
                rasa = hopfield.breeds[wynik];
                textBox11.Text = hopfield.HammingDistance(wynik).ToString();
            }
            else
                rasa = "nie rozpoznano";
            textBox10.Text = rasa;
            
            //Tabelka
            textBox12.Text = "";
            for (int i = 0; i < hopfield.breeds.Count; i++)
            {
                textBox12.Text += hopfield.breeds[i] + "  ,bład: " + hopfield.HammingDistance(i);
                textBox12.Text  += "\r\n";
            }
        }

        private void buttonRecognitionExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
