﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Imaging;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;

namespace RozpoznawanieRas
{
    public class Data
    {
        Bitmap imageLoaded;
        static byte[] loaded;
        Image image = new Image();
        private string file1 = @"C:\Users\Rafal\Desktop\Praca magisterska\Baza-rozpoznawanie twarzy\output1\";
        private string file2 = @"C:\Users\Rafal\Desktop\Praca magisterska\Baza-rozpoznawanie twarzy\output2\";
        private string file3 = @"C:\Users\Rafal\Desktop\Praca magisterska\Baza-rozpoznawanie twarzy\output3\";
        private string file4 = @"C:\Users\Rafal\Desktop\Praca magisterska\Baza-rozpoznawanie twarzy\output4\";
        private string file5 = @"C:\Users\Rafal\Desktop\Praca magisterska\Baza-rozpoznawanie twarzy\Twarze\";
        private string data = @"DaneDoTwarzy.txt";
        private string daneBT = @"DaneBezTwarzy.txt";
        private string daneT = @"DaneZTwarzami.txt";
        public Data() { }
        ~Data() { }
        public void CreateDatabase()
        {
            string[] files= {file1,file2,file3,file4,file5};
            DirectoryInfo di;
            for (int i = 0; i < files.Length; i++)
            {
                di = new DirectoryInfo(files[i]);
                foreach (FileInfo file in di.GetFiles())
                {
                    imageLoaded = (Bitmap)System.Drawing.Image.FromFile(files[i] + file);
                   if(i!=files.Length-1)
                    AddImageToMLPDatabase(imageLoaded,-1,daneBT);
                   else
                       AddImageToMLPDatabase(imageLoaded,1,daneT);
                }
            }
            StirDatabase();

        }  // służy do stworzenie pierwszej bazy 
        public void StirDatabase()
        {
            FileStream fs = new FileStream(data, FileMode.Create, FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);
            string[] linie1 = File.ReadAllLines(daneBT);
            string[] linie2 = File.ReadAllLines(daneT);
            int length = linie1.Length, j = 0, wsp = 1, k = 0;
            wsp = length / linie2.Length;
           for(int i=0 ; i<length ; i+=2)
            {
                    sw.WriteLine(linie1[i]);
                    sw.WriteLine(linie1[i+1]);
                    k++;
                if ( k % wsp == 0 && j < linie2.Length)
                {
                    sw.WriteLine(linie2[j]);
                    sw.WriteLine(linie2[j+1]);
                    j+=2;
                }
            }
            sw.Close();
            File.Delete(daneBT);
            File.Delete(daneT);
        } // przy tworzeniu bazy miesza zdjęcia i zapisuje do głównej bazy "DaneDoTwarzy" i usuwa pliki daneBT i daneT 
        //public void AddImageToDatabase(Bitmap image,string dane)
        //{
        //    char spacja = ' ';
        //    int length = 0;
        //    int pom = 0, licz = 0;
        //    image = obrazek.Modify(wczytany);
        //    wczyt = BitmapToByteArray(image);
        //    length = wczyt.Length;
        //    if (!File.Exists(dane))
        //    {
        //        using (StreamWriter sw = File.CreateText(dane)){}
        //    }
        //    using (StreamWriter writer = new StreamWriter(dane, true))
        //    {
        //        // 4 x (10x10)
        //        for (int j = 0; j < 2; j++)
        //        {
        //            for (int i = 0; i < length; i++)
        //            {
        //                if (i == 0)
        //                    i += j*10;
        //                pom++;
        //                writer.Write(wczyt[i]);
        //                writer.Write(spacja);
        //                if (pom == 10)
        //                {
        //                    i += 10;
        //                    pom = 0;
        //                }
        //            }
        //        }
        //        pom = 0;
        //        // 6 x (16x25)
        //        for (int j = 0; j < 4; j++)
        //        {
        //            for (int i = 0; i < length; i++)
        //            {
        //                if (i == 0)
        //                    i += j * 5;
        //                pom++;
        //                writer.Write(wczyt[i]);
        //                writer.Write(spacja);
        //                if (pom == 5)
        //                {
        //                    i += 15;
        //                    pom = 0;
        //                }
        //            }
        //        }
        //        pom = 0;
        //        // 6 x (6x100)
        //        for (int i = 0; i < length; i++)
        //        {
        //            pom++;
        //            licz++;
        //            writer.Write(wczyt[i]);
        //            writer.Write(spacja);
        //            if (pom == 100 && i!=length-1)
        //            {
        //                pom = 0;
        //                i -= 40;
        //            }
        //        }
        //        pom = 0;
        //        licz = 0;
        //            writer.WriteLine();
        //        writer.Close();
        //    }
        //}
        public void AddImageToMLPDatabase(Bitmap image,int face, string data)
        {
            char space = ' ';
            int length = 0;
            int pom = 0;
            image = this.image.Modify(image);
            loaded = this.image.BitmapToByteArray(image);
            length = loaded.Length;
            if (!File.Exists(data))
            {
                using (StreamWriter sw = File.CreateText(data)) { }
            }
            using (StreamWriter writer = new StreamWriter(data, true))
            {
                // 4 x (10x10)
                for (int j = 0; j < 2; j++)
                {
                    for (int i = 0; i < length; i++)
                    {
                        if (i == 0)
                            i += j * 10;
                        pom++;
                        writer.Write(loaded[i]);
                        writer.Write(space);
                        if (pom == 10)
                        {
                            i += 10;
                            pom = 0;
                        }
                    }
                }
                pom = 0;
                // 6 x (16x25)
                for (int j = 0; j < 4; j++)
                {
                    for (int i = 0; i < length; i++)
                    {
                        if (i == 0)
                            i += j * 5;
                        pom++;
                        writer.Write(loaded[i]);
                        writer.Write(space);
                        if (pom == 5)
                        {
                            i += 15;
                            pom = 0;
                        }
                    }
                }
                pom = 0;
                // 6 x (6x100)
                for (int i = 0; i < length; i++)
                {
                    pom++;
                    writer.Write(loaded[i]);
                    writer.Write(space);
                    if (pom == 100 && i != length - 1)
                    {
                        pom = 0;
                        i -= 40;
                    }
                }
                writer.WriteLine();
                writer.Write(face);
                writer.WriteLine();
                writer.Close();
            }
        }
        public void AddImageToDatabase(Bitmap image, string value, string data)
        {
            char space = ' ';
            int length = 0;
            image = this.image.ModifyRasa(image);
            loaded = this.image.BitmapToByteArray(image);
            length = loaded.Length;
            if (!File.Exists(data))
            {
                using (StreamWriter sw = File.CreateText(data)) { }
            }
            using (StreamWriter writer = new StreamWriter(data, true))
            {
                for (int i = 0; i < length; i++)
                {
                    writer.Write(loaded[i]);
                    writer.Write(space);
                }
                writer.Write(value);
                writer.WriteLine();
                writer.Close();
            }
        }
        public void SaveImageMLP(Bitmap image, string data)
        {
            char space = ' ';
            int length = 0;
            int pom = 0;
            image = this.image.Modify(image);
            loaded = this.image.BitmapToByteArray(image);
            length = loaded.Length;
            FileStream fs = new FileStream(data, FileMode.Create, FileAccess.Write);
            StreamWriter writer = new StreamWriter(fs);
            // 4 x (10x10)
            for (int j = 0; j < 2; j++)
            {
                for (int i = 0; i < length; i++)
                {
                    if (i == 0)
                        i += j * 10;
                    pom++;
                    writer.Write(loaded[i]);
                    writer.Write(space);
                    if (pom == 10)
                    {
                        i += 10;
                        pom = 0;
                    }
                }
            }
            pom = 0;
            // 6 x (16x25)
            for (int j = 0; j < 4; j++)
            {
                for (int i = 0; i < length; i++)
                {
                    if (i == 0)
                        i += j * 5;
                    pom++;
                    writer.Write(loaded[i]);
                    writer.Write(space);
                    if (pom == 5)
                    {
                        i += 15;
                        pom = 0;
                    }
                }
            }
            pom = 0;
            // 6 x (6x100)
            for (int i = 0; i < length; i++)
            {
                pom++;
                writer.Write(loaded[i]);
                writer.Write(space);
                if (pom == 100 && i != length - 1)
                {
                    pom = 0;
                    i -= 40;
                }
            }
            writer.WriteLine();
            writer.Close();
        }
        public void SaveImage(Bitmap image, string data)
        {
            char space = ' ';
            int length = 0;
            image = this.image.ModifyRasa(image);
            loaded = this.image.BitmapToByteArray(image);
            length = loaded.Length;
            FileStream fs = new FileStream(data, FileMode.Create, FileAccess.Write);
            StreamWriter writer = new StreamWriter(fs);
            for (int i = 0; i < length; i++)
            {
                writer.Write(loaded[i]);
                writer.Write(space);
            }
            writer.WriteLine();
            writer.Close();
        }
        public void AddImageFolder(string path, int face, string data)
        {
            DirectoryInfo di;
            di = new DirectoryInfo(path);
            foreach (FileInfo file in di.GetFiles())
            {
                imageLoaded = (Bitmap)System.Drawing.Image.FromFile(path + file);
                AddImageToMLPDatabase(imageLoaded, face, data);
            }
        }
        public void AddImageFolder(string path, string breed, string data)
        {
            DirectoryInfo di;
            di = new DirectoryInfo(path);
            foreach (FileInfo file in di.GetFiles())
            {
                imageLoaded = (Bitmap)System.Drawing.Image.FromFile(path + file);
                AddImageToDatabase(imageLoaded, breed, data);
            }
        }
        public int GetNumberOfLines(string dataFile)
        {
            int licznik = 0;
            using (StreamReader sr = File.OpenText(dataFile))
            {
                while ((sr.ReadLine()) != null)
                {
                    licznik++;
                }
                sr.Close();
            }
          return licznik;
        }
        
    }
}
