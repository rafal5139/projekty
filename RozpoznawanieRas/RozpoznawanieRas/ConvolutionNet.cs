﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RozpoznawanieRas
{
    class ConvolutionNet
    {
        public int NumberOfInputs { get; set; }
        public int NumberOfData { get; set; }
        public double[,] input;
        public List<double[,]> conv = new List<double[,]>();
        public List<double[,]> filters = new List<double[,]>();
        public void CreateInput()
        {
            input = new double[NumberOfData,NumberOfInputs];
        }
        public void Subsampling(int size)
        {
            int NumMaps = conv.Count();
            int x,y;
            List<double[,]> list = new List<double[,]>();
            double[,] featuresmap;
            featuresmap = conv[0];
            x = featuresmap.GetLength(0);
            y = featuresmap.GetLength(1);
            double[,] result = new double[x/2, y/2];
            for (int n = 0; n < NumMaps; n++)
            {
                featuresmap = conv[n];
                for (int i = 0; i < x/2; i++)
                    for (int j = 0; j < y/2; j++)
                    {
                        result[i, j] = (featuresmap[2 * i, 2 * j] + featuresmap[2 * i + 1, 2 * j]
                            + featuresmap[2 * i, 2 * j + 1] + featuresmap[2 * i + 1, 2 * j + 1]) / 4;
                    }
                list.Add(result);
            }
            conv = list;
        }
        public double[,] Convulation(double[,] featuresmap , double[,] filter)
        {
            int x, y,x2,y2;
            x = featuresmap.GetLength(0);
            y = featuresmap.GetLength(1);
            x2 = x - filter.GetLength(0) + 1;
            y2 = y - filter.GetLength(0) + 1;
            double[,] result = new double[x2,y2];

            for (int k = 0; k < x2; k++) // wiersze nowej tablicy
                for (int l = 0; l < y2; l++) // kolumny nowej tablicy
                {
                    for (int i = 0; i < filter.GetLength(0); i++) //wiersze filtru
                        for (int j = 0; j < filter.GetLength(1); j++) //kolumny filtru
                            result[k, l] = featuresmap[i + k, j + l] * filter[i, j];
                    result[k, l] = result[k, l] / (x2 * y2);
                }
            return result;
        }
        public void Convulation(int Numfilters)
        {
            int NumMaps = conv.Count();
            int x, y;
            List<double[,]> list = new List<double[,]>();
            double[,] featuresmap;
            featuresmap = conv[0];
            x = featuresmap.GetLength(0);
            y = featuresmap.GetLength(1);
            double[,] result = new double[x , y];
            for (int n = 0; n < NumMaps; n++)
            {
                for (int f = 0; f < Numfilters; f++)
                {
                    result = Convulation(conv[n], filters[f]);
                }
                list.Add(result);
            }
            conv = list;
        }
        public void Tanh()
        {
            int NumMaps = conv.Count();
            int x, y;
            List<double[,]> list = new List<double[,]>();
            double[,] featuresmap;
            featuresmap = conv[0];
            x = featuresmap.GetLength(0);
            y = featuresmap.GetLength(1);
            for (int n = 0; n < NumMaps; n++)
            {
                featuresmap = conv[n];
                for (int i = 0; i < x / 2; i++)
                    for (int j = 0; j < y / 2; j++)
                    {
                        featuresmap[i, j] = Math.Tanh(featuresmap[i, j]);
                    }
                list.Add(featuresmap);
            }
            conv = list;
        }
        public void Abs()
        {
            int NumMaps = conv.Count();
            int x, y;
            List<double[,]> list = new List<double[,]>();
            double[,] featuresmap;
            featuresmap = conv[0];
            x = featuresmap.GetLength(0);
            y = featuresmap.GetLength(1);
            for (int n = 0; n < NumMaps; n++)
            {
                featuresmap = conv[n];
                for (int i = 0; i < x / 2; i++)
                    for (int j = 0; j < y / 2; j++)
                    {
                        featuresmap[i, j] = Math.Abs(featuresmap[i,j]);
                    }
                list.Add(featuresmap);
            }
            conv = list;
        }
        public void Normalization(double[,] featuresmap,int newMin, int newMax)
        {
            int x = featuresmap.GetLength(0);
            int y = featuresmap.GetLength(1);
            double maxValue = MaxValue(featuresmap);
            double minValue = MinValue(featuresmap);
            for (int i = 0; i < x; i++)
                for (int j = 0; j < y; j++)
                    featuresmap[i, j] = (featuresmap[i, j] - minValue) * ((newMax - newMin) / (maxValue - minValue)) + newMin;
            }
        public double MinValue(double[,] array)
        {
            double min=array[0,0];
            int x = array.GetLength(0);
            int y = array.GetLength(1);
            for (int i = 0; i < x; i++)
                for (int j = 0; j < y; j++)
                    if (min > array[i, j])
                        min = array[i, j];
            return min;
        }
        public double MaxValue(double[,] array)
        {
            double max = array[0, 0];
            int x = array.GetLength(0);
            int y = array.GetLength(1);
            for (int i = 0; i < x; i++)
                for (int j = 0; j < y; j++)
                    if (max < array[i, j])
                        max = array[i, j];
            return max;
        }
        public double[,] GaborFilter(int size, int wavelength, int orientation, double bandwith, double ratio, double phase)
        {
            double xx,yy;
            double[,] filter = new double[size, size];
            int range = size / 2;
            for (int x = -range; x < size - range; x++)
                for (int y = -range; y < size -range; y++){
                    xx = x*Math.Cos(orientation) + y*Math.Sin(orientation);
                    yy = -x*Math.Sin(orientation) + y*Math.Cos(orientation);
                    filter[x+range, y+range] = Math.Exp(-(Math.Pow(xx, 2) + Math.Pow(yy, 2) * ratio) / (2 * Math.Pow(bandwith, 2)))
                                    * Math.Cos(2 * Math.PI * xx / wavelength+phase);
                    }
            return filter;
        }
        public double[,] LaplaceFilter(int number)
        {
            double[,] filtr;

            switch (number)
            {
                case 1:
                    filtr = new double[,] { { 0, -1, 0 }, { -1, 4, -1 }, { 0, -1, 0 } }; // wykrywa wszystkie krawędzie 
                    break;
                case 2:
                    filtr = new double[,] { { 0, -1, 0 }, { 0, 2, 0 }, { 0, -1, 0 } }; //wykrywa poziome krawędzie
                    break;
                case 3:
                    filtr = new double[,] { { 0, -1, 0 }, { 0, 2, 0 }, { 0, -1, 0 } }; // wykrywa pionowe krawędzie
                    break;
                default:
                    filtr = new double[,] { { 0, 0, 0 }, { 0, 1, 0 }, { 0, 0, 0 } }; // nic nie robi
                    break;
            }
            return filtr;
        }
       // public double[,] SobelFilter(int number)
       // {


       // }
    } 
}
