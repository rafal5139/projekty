﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using LinearAlgebra.Matricies;

namespace RozpoznawanieRas
{
   public class RBF
    {
       public int NumberOfInputs { get; set; }
       public int NumberOfFunction { get; set; }
       public int NumberOfData { get; set; }
       public int minValue { get; set; }
       public int maxValue { get; set; }
       public static string file = @"BazaRadial.txt";
       public double sigmum { get; set; }
       public DoubleMatrix Input;
       public DoubleMatrix Output;
       public DoubleMatrix Weigths;
       public DoubleMatrix G;
       public DoubleMatrix Centres;
       public Data data = new Data();
       public RBF(int function=3) {
            NumberOfFunction = function;
            minValue = 0;
            maxValue = 1;
        }
        ~RBF() { }
        public void InitializeTables()
        {
            Weigths = new DoubleMatrix(1, NumberOfFunction + 1);
            Output = new DoubleMatrix(1, NumberOfData);
            Input = new DoubleMatrix(NumberOfInputs, NumberOfData);
            Centres = new DoubleMatrix(NumberOfInputs, NumberOfFunction);
            G = new DoubleMatrix(NumberOfFunction, NumberOfData);
        }
        public void InitializeData(string data)
        {
            NumberOfData = this.data.GetNumberOfLines(data) / 2;
            GetNumberOfInputs(data);
            InitializeTables();
            LoadData(data);
        }
        public void GetNumberOfInputs(string data)
        {
            int sum = 0;
            using (StreamReader sr = File.OpenText(data))
            {
                string[] tab = new string[5];
                string s = "";
                if ((s = sr.ReadLine()) != null)
                {
                    tab = s.Split(' ');
                    if (tab[tab.Length - 1] == "")
                        sum = tab.Length - 1;
                    else
                        sum = tab.Length;
                    NumberOfInputs = sum;
                }
                sr.Close();
            }
        }
        public void LoadData(string data)
        {
            using (StreamReader sr = File.OpenText(data))
            {
                string[] tab = new string[5];
                string s = "";
                int i = 0;
                while ((s = sr.ReadLine()) != null && i != NumberOfData)
                {
                    tab = s.Split(' ');
                    for (int j = 0; j < NumberOfInputs; j++)
                    {
                        Input[j,i] = double.Parse(tab[j]);
                    }
                    if ((s = sr.ReadLine()) != null)
                    {
                        tab = s.Split(' ');
                        Output[0,i] = double.Parse(tab[0]);
                    }
                    i++;
                }
                sr.Close();
            }
        }
        public void Train()
        {
            InitializeData(file);
            CalculateCentres();
            G = EuklidesDistance(Input, Centres);
            CalculateSigma();
            G=GaussFunction(G);
            G=AddBias();
            CalculateWeights();

            //póki bład nie bedzie wystarczająco mały 
            double error = 0.5, error_1 = GetError();
            int counter = 0;
            while ((error_1 > error && error_1 == Double.NaN) || counter <= 100)
            {
                CalculateCentres();
                G = EuklidesDistance(Input, Centres);
                CalculateSigma();
                G = GaussFunction(G);
                G = AddBias();
                CalculateWeights();
                counter++;
                error_1 = GetError();
            } 
        }
        public double GetError()
        {
            double error = 0;
            DoubleMatrix outp = new DoubleMatrix(1, NumberOfData);
            outp = G * Weigths;
            //for (int i = 0; i < LiczbaDanych; i++)
            //    error += Math.Pow(outp[0, i] - Output[0, i], 2);
            //error = Math.Sqrt(error);
            for (int i = 0; i < NumberOfData; i++)
            {
                if(outp[0, i]- Output[0,i] < 0 && Output[0, i]==1)
                    error += Math.Pow(outp[0, i] - Output[0, i], 2);
                if(outp[0, i]- Output[0,i] > 0 && Output[0, i]==0)
                    error += Math.Pow(outp[0, i] - Output[0, i], 2);

            }
            error = Math.Sqrt(error);
            return error;
        }

        public void CalculateWeights()
        {
            Weigths = G.PseudoInverse*Output;
        }
        public double Run()
        {
            double result = 0;
            string data = @"TestRadial.txt";
            NumberOfData = this.data.GetNumberOfLines(data);
            GetNumberOfInputs(data);
            Input = new DoubleMatrix(NumberOfInputs,NumberOfData);
            G = new DoubleMatrix(NumberOfFunction + 1,NumberOfData);
            LoadData(data);
            
            G = EuklidesDistance(Input, Centres);
            G = GaussFunction(G);
            G = AddBias();
            result = CalculateOutputs();
            return result;
        }
        public double CalculateOutputs()
        {
            Output = G * Weigths;
            return Output[0,0];
        }
        public DoubleMatrix EuklidesDistance(DoubleMatrix x1, DoubleMatrix x2)
        {
            int col,row,num;
            col = x2.RowCount;
            row = x1.RowCount;
            num = x1.ColumnCount;
            DoubleMatrix result = new DoubleMatrix(col,row);
            for(int k=0;k < row;k++)
              for(int i=0;i<col;i++)
                for(int j=0; j<num;j++)
                    result[i,k] += Math.Pow((x1[j,k] - x2[j,i]), 2);
         
            for(int j=0;j<col;j++)
              for(int i=0;i<row;i++)
                 result[j,i] = Math.Sqrt(result[j,i]);
            return result;
       }
        public DoubleMatrix GaussFunction(DoubleMatrix x)
        {
            int col,row;
            col = x.ColumnCount;
            row= x.RowCount;
            for (int i = 0; i < row; i++)
                for (int j = 0; j < col; j++)
                    x[j, i] = Math.Exp((-1)*Math.Pow(x[j, i], 2)/(2*sigmum*sigmum));
            return x;
       }
        public void RandomCentres()
        {
            Random rand = new Random();
            MaxMinValues();
            for (int i = 0; i < NumberOfFunction; i++)
                for (int j = 0; j < NumberOfInputs; j++)
                    if (Math.Abs(maxValue - minValue) <= 5)
                        Centres[j, i] = rand.Next(minValue, maxValue) + rand.NextDouble();
                    else
                        Centres[j, i] = rand.Next(minValue, maxValue);
            //Centres[0, 0] = 1;
            //Centres[1, 0] = 1;
            //Centres[0, 1] = 0;
            //Centres[1, 1] = 0;
        }
        public void CalculateCentres()
        {
            double min=0;
            int poz=0;
            List<int>[] concentration = new List<int>[NumberOfFunction];
            for (int i = 0; i < NumberOfFunction; i++)
                concentration[i] = new List<int>();
            List<int>[] concentration_prev = new List<int>[NumberOfFunction];
            for (int i = 0; i < NumberOfFunction; i++)
                concentration_prev[i] = new List<int>();
            DoubleMatrix distances = new DoubleMatrix(NumberOfFunction, NumberOfData);           
            RandomCentres();

            do{
            distances = EuklidesDistance(Input, Centres);
            concentration_prev = ClearList(concentration_prev);
            for (int i = 0; i < NumberOfFunction; i++)
                for (int j = 0; j < concentration[i].Count; j++)
                    concentration_prev[i].Add(concentration[i][j]);
            concentration = ClearList(concentration);
            for (int i = 0; i < NumberOfData; i++)
            {
                min = distances[0, i];
                poz = 0;
                for (int j = 0; j < NumberOfFunction; j++)
                    if (distances[j, i] < min)
                    {
                        min = distances[j, i];
                        poz = j;
                    }
                concentration[poz].Add(i);
            }
            ModifyCentres(concentration);
            }while(!ConcentrationChanged(concentration,concentration_prev) );
        }
        public void MaxMinValues()
        {
            minValue = Convert.ToInt32(Input.MinimumValue);
            maxValue = Convert.ToInt32(Input.MaximumValue);
        }
        public List<int>[] ClearList(List<int>[] s)
        {
            for (int i = 0; i < NumberOfFunction; i++)
                for (int j = 0; j < s[i].Count; j++)
                    s[i].Clear();
            return s;
        }
        public bool ConcentrationChanged(List<int>[] s, List<int>[] s_prev)
       {
           bool equal = false;

           for (int i=0; i < NumberOfFunction; i++)
               if (s[i].Count != s_prev[i].Count)
                   return false;

           for (int i = 0; i < NumberOfFunction; i++)
               for (int j = 0; j < s[i].Count; j++)
                   for (int k = 0; k < s[i].Count; k++)
                   {
                       if (s[i][j] == s[i][k])
                       {
                           equal = true;
                           k = s[i].Count;
                       }
                       if (k == s[i].Count - 1)
                           return false;
                   }

           return equal;
       }
        public void ModifyCentres(List<int>[] concentration)
        {
            //zerowanie centrów
            for (int i = 0; i < Centres.RowCount; i++)
                for (int j = 0; j < Centres.ColumnCount; j++)
                {
                    if(concentration[i].Count>0)
                       Centres[j, i] = 0;
                }
            //wyliczanie nowych wartości
            for(int i=0; i < NumberOfFunction; i++)
               for (int k = 0; k < concentration[i].Count; k++)
                for (int j = 0; j < NumberOfInputs; j++)
                {
                    Centres[j, i] += Input[j, concentration[i][k]] / concentration[i].Count;
                }
        }
        public void CalculateSigma()
        {
            sigmum = MaxCenterDistance() / (Math.Sqrt(2 * NumberOfFunction));
        }
        public double MaxCenterDistance()
        {
            double max = 0;
            DoubleMatrix distances = new DoubleMatrix(NumberOfFunction,NumberOfFunction);
            distances = EuklidesDistance(Centres, Centres);
          for(int i=0;i<NumberOfFunction;i++)
              for (int j = 0; j < NumberOfFunction; j++)
              {
                  if (distances[j, i] > max)
                      max = distances[j, i];
              }
          return max;
        }
        public DoubleMatrix AddBias()
        {
            int row,col;
            col = G.ColumnCount+1;
            row = G.RowCount;
            DoubleMatrix result = new DoubleMatrix(col,row);
            
            for(int i=0;i<row;i++)
                for (int j = 0; j < col; j++)
                {
                    if (j == col - 1)
                        result[j,i] = 1;
                    else
                    result[j,i] = G[j,i];
                }
            return result;
        }

    }
}
