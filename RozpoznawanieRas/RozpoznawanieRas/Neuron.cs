﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RozpoznawanieRas
{
    class Neuron
    {
        int number=0;
        public double[] inputs;
        public double[] weights;
        public double[] weights1; // przechowuje wagi z (j-1) epoki
        public double[] weights2; // zapamietuje wagi z (j-1) epoki
        public double error=0;
        public Neuron(int value)
        {
            number = value;
            CreateInput();
            CreateWeights();
            CreateWeights1();
            CreateWeights2();
            randomizeWeights();
        }
        public double biasWeight;
        //private double biasWeightM;

        private Random r = new Random();
        public void CreateInput()
        {
            inputs = new double[number];
        }
        public void CreateWeights()
        {
            weights = new double[number];
        }
        public void CreateWeights1()
        {
            weights1 = new double[number];
            for (int i = 0; i < number; i++)
                weights1[i] = 0;
        }
        public void CreateWeights2()
        {
            weights2 = new double[number];
            for (int i = 0; i < number; i++)
                weights2[i] = 0;
        }
        public void SaveWeights2()
        {
            for(int i=0;i<number;i++)
            weights2[i] = weights[i];
        } // przepisuje wagi z weights do weights2
        public void SaveWeights1()
        {
            for (int i = 0; i < number; i++)
            weights1[i] = weights2[i];
        } // przepisuje wagi z weights2 do weights1
        public double Function(double x)
        {
            return 2.0 / (1.0 + Math.Exp(-x))-1;
        }
        public double output()
        {
            double sum = 0;
            for (int i = 0; i < number; i++)
                sum += weights[i] * inputs[i];
            return Function(sum + biasWeight);
        }
        public void randomizeWeights()
        {
            for (int i = 0; i < number; i++)
                weights[i] = r.NextDouble()/5 - 0.1;
            biasWeight = 1;
        }

        public void adjustWeights(double eta, double momentum)
        {
            //double l=0, delta = 0;
            //l = eta * error;
            //delta = momentum * (weights1[0] - weights[0]);
            for (int i = 0; i < number; i++)
            {
                //delta = momentum * (weights[i] - weights1[i]);
                weights[i] += eta * error * inputs[i];// +delta;
            }
            biasWeight += eta * error;
        }
    }
}
