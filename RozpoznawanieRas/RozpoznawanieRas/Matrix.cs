﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearAlgebra.Matricies;
namespace RozpoznawanieRas
{
    public class Matrix
    {
      //public  DoubleMatrix mtx = new DoubleMatrix(5, 5);

        public static double[] MatrixProduct(double[][] matrixA, double[] vectorB)
        {
            int aRows = matrixA.Length; int aCols = matrixA[0].Length;
            int bRows = vectorB.Length;
            if (aCols != bRows)
                throw new Exception("Wymiary sie nie zgadzaja");
            double[] result = new double[aRows];
            for (int i = 0; i < aRows; ++i) // each row of A
                for (int k = 0; k < aCols; ++k)
                    result[i] += matrixA[i][k] * vectorB[k];
            return result;
        }
        public static double[][] MatrixProduct(double[][] matrixA, double[][] matrixB)
        {
            int aRows = matrixA.Length; int aCols = matrixA[0].Length;
            int bRows = matrixB.Length; int bCols = matrixB[0].Length;
            if (aCols != bRows)
                throw new Exception("Wymiary macierzy sie nie zgadzaja");
            double[][] result = MatrixCreate(aRows, bCols);
            for (int i = 0; i < aRows; ++i) // each row of A
                for (int j = 0; j < bCols; ++j) // each col of B
                    for (int k = 0; k < aCols; ++k)
                        result[i][j] += matrixA[i][k] * matrixB[k][j];
            return result;
        }
        public static double[][] MatrixCreate(int rows, int cols)
        {
            // creates a matrix initialized to all 0.0s  
            // do error checking here?  
            double[][] result = new double[rows][];
            for (int i = 0; i < rows; ++i)
                result[i] = new double[cols];
            // auto init to 0.0  
            return result;
        }
        public static double[][] MatrixTranspose(double[][] matrixA)
        {
            int aRows = matrixA.Length; int aCols = matrixA[0].Length;
            double[][] result = MatrixCreate(aCols,aRows);
            for (int i = 0; i < aRows; i++)
                for (int j = 0; j < aCols; j++)
                    result[j][i] = matrixA[i][j];
            return result;
        }
        public double[][] IdentityMatrixCreate(int rows, int cols)
        {
            double[][] result = new double[rows][];
            for (int i = 0; i < rows; ++i)
                result[i] = new double[cols];

            for(int i=0;i<rows;i++)
                for (int j = 0; j < cols; j++)
                {
                    if (i == j)
                        result[i][j] = 1;
                    else
                        result[i][j] = 0;
                }
            return result;
        }
        public double[][] MatrixInverse(double[][] matrixA)
        {
            int aRows = matrixA.Length; int aCols = matrixA[0].Length;
            if (aCols != aRows)
                throw new Exception("Macierz nie jest prostokatna!");
            double[][] result = IdentityMatrixCreate(aCols, aRows);
            double[,] tab1 = new double[,] { { 2,2 } };
            double[,] tab2 = new double[,] { { 3 },{2}};
            DoubleMatrix matrix1 = new DoubleMatrix(tab1);
            DoubleMatrix matrix2 = new DoubleMatrix(tab2);
            DoubleMatrix matrix3 = matrix1 * matrix2;
            matrix2 = matrix1.PseudoInverse;
            return result;
        }
    }
}
