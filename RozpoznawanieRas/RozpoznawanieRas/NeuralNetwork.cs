﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
    
using Accord;
//using Accord.Neuro.Learning;
//using AForge.Neuro;
//using AForge.Neuro.Learning;

namespace RozpoznawanieRas
{
    public class MLP:Net
    {
        public int iteration = 100;
        public static int N0=1400;
        public static int N1 = 52;
        public double eta=0.6,momentum=0.5,error=0;
        public double[][] output;
        public int numberOfData;
        public static string file = @"DaneDoTwarzy.txt";
        public MLP()
        {
            InitializeNeurons();
        }
        ~MLP() { }
        Neuron[] neurons = new Neuron[N1];
        Neuron outputNeuron = new Neuron(N1);
        public void CreateInput()
        {
            input = new double[numberOfData][];
            for (int i = 0; i < numberOfData; i++)
                input[i] = new double[N0];
        }
        public void CreateOutput()
        {
            output = new double[numberOfData][];
            for (int i = 0; i < numberOfData; i++)
                output[i] = new double[1];
        }
        public void InitializeNeurons()
        {
            //tworzenie neuronów - potrzeba 26/52 ukrytych i 1 wyjściowy
            for (int i = 0; i < N1; i++)
            {
                if (i >= 0 && i < 4 * N1 / 26 || i >= 20 * N1 / 26)
                    neurons[i] = new Neuron(100);
                else
                    neurons[i] = new Neuron(25);
            }
        }
        override public void InitializeData(string dane)
        {
            numberOfData = data.GetNumberOfLines(dane) / 2;
            CreateInput();
            CreateOutput();
            LoadData(dane);
        }
        public double DerivativeFunct(double x)
        {
            return (1 - x*x)/2;
        }
        override public void LoadData(string dane)
        {
            using (StreamReader sr = File.OpenText(dane))
            {
                string[] tab = new string[5];
                string s = "";
                int i = 0;
                while ((s = sr.ReadLine()) != null && i != numberOfData)
                {
                    tab = s.Split(' ');
                    for (int j = 0; j < N0; j++)
                        input[i][j] = double.Parse(tab[j]) / 255;
                    if ((s = sr.ReadLine()) != null)
                    {
                        tab = s.Split(' ');
                        output[i][0] = int.Parse(tab[0]);
                    }
                    i++;
                }
                sr.Close();
            }
        }
        public void CalculateOutputs(int j)
        {
            int l = 0;
            //przypisanie neurona wartosci
            for (int k = 0; k < N1; k++)
            {
                if (k >= 0 && k < 4 * N1 / 26 || k >= 20 * N1 / 26)
                {
                    if (k % 2 == 1 && N1==52)
                        l -= 100;
                    for (int i = 0; i < 100; i++)
                    {
                        neurons[k].inputs[i] = input[j][l];
                        l++;
                    }
                }
                else
                {
                    if (k % 2 == 1 && N1==52)
                        l -= 25;
                    for (int i = 0; i < 25; i++)
                    {
                        neurons[k].inputs[i] = input[j][l];
                        l++;
                    }
                }
             }

            l = 0;
            //teraz neuron wyjsciowy
            for (int i = 0; i < N1; i++)
                outputNeuron.inputs[i] = neurons[i].output();
        }
        public double Train(string file)
        {
            int epoch = 0, i, j;
           // double error2 = 0;
            File.Delete(@"Errors.txt");
            InitializeData(file);
            //for (i = 0; i < N1; i++)
            //{
            //    neurony[i].ZapamiętajWagi2();
            //    neurony[i].ZapamiętajWagi1();
            //}
            while (epoch < iteration || error < 0.01)
            {
                //if (error > error2 && Math.Abs(error-error2) < 1)
                //    eta = eta * 0.9;
                //if (error > error2 && Math.Abs(error - error2) > 1)
                //    eta = eta * 1.1;
                //if (error < error2 && Math.Abs(error - error2) < 1)
                //    eta= eta * 1.1;
                //if (error < error2 && Math.Abs(error - error2) > 1)
                //    eta = eta * 0.9;
                //error2 = error;

                //if (epoch == 200)
                //    eta = 0.7;
                //if (epoch == 400)
                //    eta = 0.5;
                //if (epoch == 800)
                //    eta = 0.4;
                //if (epoch == 1200)
                //    eta = 0.3;
                //if (epoch == 1400)
                //    eta = 0.2;
                //if (epoch == 1600)
                //    eta = 0.1;
                error = 0;
                for (j = 0; j < numberOfData; j++)
                {
                    CalculateOutputs(j);
                    
                    outputNeuron.error =(output[j][0] - outputNeuron.output())* DerivativeFunct(outputNeuron.output());
                    error += Math.Abs(output[j][0] - outputNeuron.output());
                    outputNeuron.adjustWeights(eta,momentum);

                    for (i = 0; i < N1; i++)
                    {
                        neurons[i].error = DerivativeFunct(neurons[i].output()) * outputNeuron.error * outputNeuron.weights[i];
                        //neurony[i].ZapamiętajWagi2();
                    }
                    for (i = 0; i < N1; i++)
                    {
                        neurons[i].adjustWeights(eta, momentum);
                        //neurony[i].ZapamiętajWagi1();
                    }

                }
                epoch++;
                SaveErrors(error/(numberOfData));               
            }
            return error/numberOfData;
        } // dla nowych danych 
        override public void Train()    // dla tych samych danych - nie można użyć po testowaniu 
        {
            int epoch = 0, i, j;
            File.Delete(@"Errors.txt");            
            while (epoch < 2000)
            {
                if (epoch == 200)
                    eta = 0.1;
                if (epoch == 400)
                    eta = 0.1;
                if (epoch == 600)
                    eta = 0.1;
                if (epoch == 800)
                    eta = 0.01;
                for (j = 0; j < numberOfData; j++)
                {
                    CalculateOutputs(j);

                    outputNeuron.error = (output[j][0] - outputNeuron.output()) * DerivativeFunct(outputNeuron.output());
                    outputNeuron.adjustWeights(eta, momentum);

                    for (i = 0; i < N1; i++)
                        neurons[i].error = DerivativeFunct(neurons[i].output()) * outputNeuron.error * outputNeuron.weights[i];
                    for (i = 0; i < N1; i++)
                    {
                        neurons[i].adjustWeights(eta, momentum);
                        neurons[i].SaveWeights1();
                    }
                }
                epoch++;
                SaveErrors(outputNeuron.error);
            }
        }
        override public double TestNet(string test) // uzywanmy na samym poczatku i po testowaniu żeby wczytac dane z bazy
        {
            InitializeData(test);
            double result, points = 0, pom = 0; ;
            for(int j=0;j<numberOfData;j++)
            {
                CalculateOutputs(j);
                pom = outputNeuron.output();
            if (Math.Abs(pom - output[j][0]) < 1)
                points++;
            }
            result =  100*points /numberOfData;
            return result;
        }
        public void SaveErrors(double error)
        {
            if (!File.Exists(@"Errors.txt"))
            {
                using (StreamWriter sw = File.CreateText(@"Errors.txt")) { }
            }
            using (StreamWriter writer = new StreamWriter(@"Errors.txt", true))
            {
                writer.Write(error);
                writer.WriteLine();
                writer.Close();
            }
        }
        override public double Run(Bitmap image)
        {
            double wynik = 0;
            data.SaveImageMLP(image, @"ZdjecieTwarzy.txt");
            numberOfData = 1;
            CreateInput();
            LoadData(@"ZdjecieTwarzy.txt");
            CalculateOutputs(0);
            wynik = outputNeuron.output();
            return wynik;
        }
        override public void SaveNet(string siec)
        {
            FileStream fs = new FileStream(siec, FileMode.Create, FileAccess.Write);
            StreamWriter writer = new StreamWriter(fs);
            char spacja = ' ';
            for (int k = 0; k < N1; k++)
            {
                if (k >= 0 && k < 4 * N1 / 26 || k >= 20 * N1 / 26)
                {
                    for (int i = 0; i < 100; i++)
                    {
                        writer.Write(neurons[k].weights[i]);
                        writer.Write(spacja);
                    }
                    writer.Write(neurons[k].biasWeight);
                    writer.WriteLine();
                }
                else
                {
                    for (int i = 0; i < 25; i++)
                    {
                        writer.Write(neurons[k].weights[i]);
                        writer.Write(spacja);
                    }
                    writer.Write(neurons[k].biasWeight);
                    writer.WriteLine();
                }
            }
            for (int i = 0; i < N1; i++)
            {
                writer.Write(outputNeuron.weights[i]);
                writer.Write(spacja);
            }
            writer.Write(outputNeuron.biasWeight);
            writer.Close();       
        }
        override public void LoadNet(string siec)
        {
            numberOfData = data.GetNumberOfLines(siec);
            N1 = numberOfData - 1;
            InitializeNeurons();

            using (StreamReader sr = File.OpenText(siec))
            {
                string[] tab = new string[5];
                string s = "";
                int  k = 0;
                while ((s = sr.ReadLine()) != null && k != numberOfData)
                {
                    tab = s.Split(' ');
                    if (k != N1)
                        if (k >= 0 && k < 4 * N1 / 26 || k >= 20 * N1 / 26)
                        {
                            for (int j = 0; j < 100; j++)
                                neurons[k].weights[j] = double.Parse(tab[j]);
                            neurons[k].biasWeight = double.Parse(tab[100]);
                        }
                        else
                        {
                            for (int j = 0; j < 25; j++)
                                neurons[k].weights[j] = double.Parse(tab[j]);
                            neurons[k].biasWeight = double.Parse(tab[25]);
                        }
                    else
                    {
                        for (int j = 0; j < N1; j++)
                            outputNeuron.weights[j] = double.Parse(tab[j]);
                        outputNeuron.biasWeight = double.Parse(tab[N1]);
                    }
                    k++;
                }
                sr.Close();
            }
        }
        public void ChangeSetting(int znak)
        {
                //if (0.1*znak+ momentum < 1)
                   // momentum = momentum * 0.1*znak;
                //eta = eta *0.1*znak;
            momentum = 0.8;
        }

    }
}
