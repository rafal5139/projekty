﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WykrywanieKrawedzi
{
    class EdgeDetection
    {

        public double[,] GaborFilter(int size, int wavelength, int orientation, double bandwith, double ratio, double phase)
        {
            double xx, yy;
            double[,] filter = new double[size, size];
            int range = size / 2;
            for (int x = -range; x < size - range; x++)
                for (int y = -range; y < size - range; y++)
                {
                    xx = x * Math.Cos(orientation) + y * Math.Sin(orientation);
                    yy = -x * Math.Sin(orientation) + y * Math.Cos(orientation);
                    filter[x + range, y + range] = Math.Exp(-(Math.Pow(xx, 2) + Math.Pow(yy, 2) * ratio) / (2 * Math.Pow(bandwith, 2)))
                                    * Math.Cos(2 * Math.PI * xx / wavelength + phase);
                }
            return filter;
        }
        public double[,] LaplaceFilter(int number)
        {
            double[,] filtr;

            switch (number)
            {
                case 1:
                    filtr = new double[,] { { 0, -1, 0 }, { -1, 4, -1 }, { 0, -1, 0 } }; // wykrywa wszystkie krawędzie 
                    break;
                case 2:
                    filtr = new double[,] { { 0, -1, 0 }, { 0, 2, 0 }, { 0, -1, 0 } }; //wykrywa poziome krawędzie
                    break;
                case 3:
                    filtr = new double[,] { { 0, -1, 0 }, { 0, 2, 0 }, { 0, -1, 0 } }; // wykrywa pionowe krawędzie
                    break;
                default:
                    filtr = new double[,] { { 0, 0, 0 }, { 0, 1, 0 }, { 0, 0, 0 } }; // nic nie robi
                    break;
            }
            return filtr;
        }
    }
}
