﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WykrywanieKrawedzi
{
    public partial class Form1 : Form
    {
        String file;
        public Form1()
        {
            InitializeComponent();
        }
        private void LoadPicture(String file)
        {
            pictureBox1.Image = (Bitmap)System.Drawing.Image.FromFile(file);
        }
        private void buttonLoad_Click(object sender, EventArgs e)
        {

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    file = openFileDialog1.FileName;
                    LoadPicture(file);
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show("Invalid image: " + ex.Message, "Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch
                {
                    MessageBox.Show("Failed loading the image", "Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
