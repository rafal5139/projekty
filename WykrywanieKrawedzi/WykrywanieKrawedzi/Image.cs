﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AForge.Imaging.Filters;
using AForge;
using AForge.Imaging;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace WykrywanieKrawedzi
{
    class Image
    {
        int width { get; set; }
        int heigth { get; set; }
        public Image()
        {

        }
        ~Image() { }
        public Bitmap ResizeImage(Bitmap imgToResize, int width, int height)
        {
            ResizeBilinear filter = new ResizeBilinear(width, height);          
            Bitmap newImage = filter.Apply(imgToResize);
            return newImage;
        }
        public Bitmap HistEqual(Bitmap imgToEqual)
        {
            HistogramEqualization filter = new HistogramEqualization();
            filter.ApplyInPlace(imgToEqual);
            return imgToEqual;
        }
        public Bitmap RotateImg(Bitmap imgToRotate, int angle)
        {
            // create filter - rotate for 30 degrees keeping original image size
            RotateBicubic filter = new RotateBicubic(angle, true);
            
            Bitmap newImage = filter.Apply(imgToRotate);
            return newImage;
        }
        public Bitmap Greyscale(Bitmap imgtoGrey)
        {
            Grayscale filter = new Grayscale(0.2125, 0.7154, 0.0721);
            
            Bitmap grayImage = filter.Apply(imgtoGrey);
            return grayImage;
        }
        public Bitmap Normalization(Bitmap imgtoNorm)
        {
            ContrastStretch filter = new ContrastStretch();
            filter.ApplyInPlace(imgtoNorm);
            return imgtoNorm;
        }
        public Bitmap Sharpen(Bitmap imgToSharp)
        {
            Sharpen filter = new Sharpen();         
            filter.ApplyInPlace(imgToSharp);
            return imgToSharp;
        }
        public Bitmap BrightnessCorrection(Bitmap imgToCorrect, int value)
        {
            BrightnessCorrection filter = new BrightnessCorrection(value);
            filter.ApplyInPlace(imgToCorrect);
            return imgToCorrect;
        }
        public Bitmap BlackAndWhite(Bitmap imgToBinary)
        {
            OtsuThreshold filter = new OtsuThreshold();    
            filter.ApplyInPlace(imgToBinary);
            return imgToBinary;
        }
        public Bitmap Dithering(Bitmap image)
        {
            FloydSteinbergDithering filter = new FloydSteinbergDithering();         
            filter.ApplyInPlace(image);
            return image;
        }
        public byte[] BitmapToByteArray(Bitmap bitmap)
        {
            BitmapData bmpdata = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, bitmap.PixelFormat);
            int numbytes = bmpdata.Stride * bitmap.Height;
            byte[] bytedata = new byte[numbytes];
            IntPtr ptr = bmpdata.Scan0;
            Marshal.Copy(ptr, bytedata, 0, numbytes);
            bitmap.UnlockBits(bmpdata);
            return bytedata;
        }
    }
}
